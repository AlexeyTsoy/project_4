﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WorldMap : MonoBehaviour
{
	public TweenAlpha tA;
	public TweenAlpha fade;
	public UILabel info;

	void Start ()
	{

	}
	
	public void Play()
	{
		SceneManager.LoadScene("Level_1-1");
	}
	public void LocOneInfo()
	{
		tA.PlayForward();
		fade.PlayForward();
		info.text = "This is the first level";
	}

}
