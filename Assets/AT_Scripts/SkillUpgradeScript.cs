﻿using UnityEngine;
using System.Collections;

public class SkillUpgradeScript : MonoBehaviour 
{
	public UILabel runeTitle;
	public UILabel skillPrice;
	public UILabel skillLevel;

	public void SkillsUpgrade()
	{
		switch (runeTitle.text) 
		{
		case "Damage":
			Skills.fireDOTrune1Level ++;
			skillPrice.text = "Price: ";
			skillLevel.text = "Level: " + Skills.fireDOTrune1Level;
			break;
		case "Double Attack":
			Skills.fireDOTrune2Level ++;
			skillPrice.text = "Price: ";
			skillLevel.text = "Level: " + Skills.fireDOTrune2Level;
			break;
		case "Explosion":
			Skills.fireDOTrune3Level ++;
			skillPrice.text = "Price: ";
			skillLevel.text = "Level: " + Skills.fireDOTrune3Level;
			break;
		case "Critical Strike":
			Skills.extraCritChanceRune1Level ++;
			skillPrice.text = "Price: ";
			skillLevel.text = "Level: " + Skills.extraCritChanceRune1Level;
			break;
		case "Rune2":
			Skills.extraCritChanceRune2Level ++;
			skillPrice.text = "Price: ";
			skillLevel.text = "Level: " + Skills.extraCritChanceRune2Level;
			break;
		case "Rune3":
			Skills.extraCritChanceRune3Level ++;
			skillPrice.text = "Price: ";
			skillLevel.text = "Level: " + Skills.extraCritChanceRune3Level;
			break;
		}
	}
}
