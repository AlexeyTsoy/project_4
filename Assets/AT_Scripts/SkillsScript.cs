﻿using UnityEngine;
using System.Collections;

public class SkillsScript : MonoBehaviour 
{
	public float extraCritChance, m_Radius;

	public int localCritTime;

	EnemyScript enemy;

	void Awake()
	{
	}

	void Start()
	{
		enemy = GetComponent<EnemyScript>();
	}

	void Update()
	{
	}

	public void ExtraCritChanceMethod()
	{
		enemy.critTime--;
		extraCritChance = 100 + (Skills.extraCritChanceRune1Level * 0.03f);
		Transform spell = enemy.statusBar.transform.Find("extraCrit");
		spell.GetComponentInChildren<UILabel>().text = enemy.critTime.ToString();
		if(enemy.critTime <= 0)
		{
			extraCritChance = 0;
			enemy.critTime = 2;
			Destroy(spell.gameObject);
			enemy.statusBar.GetComponent<UIGrid>().repositionNow = true;
			Global.wind -= ExtraCritChanceMethod;
		}
		print ("extraCrit");
	}

	IEnumerator SkillDamage(float multiplierDamage, bool isDoubleHit, bool isAOE, float time)
	{
		if(isDoubleHit)
		{
			if(isAOE)
			{
				enemy.ReceiveDamageAOE(multiplierDamage, true, ElementType.Fire);
				yield return new WaitForSeconds(time);
				enemy.ReceiveDamageAOE(multiplierDamage, true, ElementType.Fire);
			}
			else
			{
				enemy.ReceiveDamage(multiplierDamage, true, ElementType.Fire);
				yield return new WaitForSeconds(time);
				enemy.ReceiveDamage(multiplierDamage, true, ElementType.Fire);	
			}
		}
		else
		{
			yield return new WaitForSeconds(time);
			Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.5f);
			foreach(Collider col in hitColliders)
			{
				if(col.GetComponent<SkillsScript>() != null)
				{
					if(isAOE)
					{
						col.GetComponent<EnemyScript>().ReceiveDamageAOE(multiplierDamage, true, ElementType.Fire);
					}
					else
					{
						col.GetComponent<EnemyScript>().ReceiveDamage(multiplierDamage, true, ElementType.Fire);
					}
				}
			}
		}
	}

	public void FireDOT()
	{
		enemy.fireDOTtime--;
		Transform spell = enemy.statusBar.transform.Find("fireDOT");
		spell.GetComponentInChildren<UILabel>().text = enemy.fireDOTtime.ToString();
		float multiplierDamage = 1 + (Skills.fireDOTrune1Level * 0.05f);

		if(Skills.fireDOTrune2Level > 0)
		{
			int doubleHitChance = Skills.fireDOTrune2Level * 10;
			int doubleHitRandom = Random.Range(1, 100);
			bool doubleHit = doubleHitRandom <= doubleHitChance ? true:false;
			if(doubleHit)
			{
				StartCoroutine(SkillDamage(multiplierDamage, true, false, 0.2f));
			}
			else
			{
				enemy.ReceiveDamage(multiplierDamage, true, ElementType.Fire);
			}
		}
		else
		{
			enemy.ReceiveDamage(multiplierDamage, true, ElementType.Fire);
		}

		if(enemy.fireDOTtime <= 0)
		{
			if(Skills.fireDOTrune3Level > 0)
			{
				StartCoroutine(SkillDamage(1 + (Skills.fireDOTrune3Level * 0.3f), false, false, Random.Range(0.1f, 1)));
			}
			enemy.fireDOTtime = 3;
			Destroy(spell.gameObject);
			enemy.statusBar.GetComponent<UIGrid>().repositionNow = true;
			Global.fireDOT -= FireDOT;
		}
		print ("fireDOT");
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, m_Radius);
	}

	public void AOEfireDOT()
	{
		enemy.AOEfireDOTtime--;
		Transform spell = enemy.statusBar.transform.Find("AOEfireDOT");
		spell.GetComponentInChildren<UILabel>().text = enemy.AOEfireDOTtime.ToString();
		float multiplierDamage = 1 + (Skills.fireDOTrune1Level * 0.05f);
		if(Skills.fireDOTrune2Level > 0)
		{
			int doubleHitChance = Skills.fireDOTrune2Level * 10;
			int doubleHitRandom = Random.Range(1, 100);
			bool doubleHit = doubleHitRandom <= doubleHitChance ? true:false;
			if(doubleHit)
			{
				StartCoroutine(SkillDamage(multiplierDamage, true, true, 0.5f));
			}
			else
			{
				enemy.ReceiveDamageAOE(multiplierDamage, true, ElementType.Fire);
			}
		}
		else
		{
			enemy.ReceiveDamageAOE(multiplierDamage, true, ElementType.Fire);
		}
		if(enemy.AOEfireDOTtime <= 0)
		{
			StartCoroutine(SkillDamage(1 + (Skills.fireDOTrune3Level * 0.3f), false, true, Random.Range(0.1f, 1)));
			enemy.AOEfireDOTtime = 3;
			Destroy(spell.gameObject);
			enemy.statusBar.GetComponent<UIGrid>().repositionNow = true;
			Global.AOEfireDOT -= AOEfireDOT;
		}
		print ("AOEfireDOT");
	}
}
