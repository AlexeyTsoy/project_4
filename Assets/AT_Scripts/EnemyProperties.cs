using UnityEngine;
using System.Collections;

public class EnemyProperties : MonoBehaviour 
{
	public TweenAlpha infoTA;
	public TweenAlpha spriteTA;

	public UILabel info;

	EnemyScript enemy;

	void Start()
	{
		enemy = GetComponentInParent<EnemyScript>();
	}

	void OnPress(bool isPressed)
	{
		if(isPressed)
		{
			if(name == "features")
			{
				spriteTA.PlayForward();
				infoTA.PlayForward();
				info.text = 
				"Damage: " + (int)enemy.currentDamage +
				"\nDmgType : " + enemy.enemyDamageType +
				"\nWeakness : " + enemy.weakness +
				"\nDodge : " + (int)enemy.dodgeChance + "%" +
				"\nCritical Strike : " + (int)enemy.critChance + "%" + 
				"\nAccuracy: " + (int)enemy.accuracy + "%" +
				"\nDefence: " + (int)enemy.critChance + "%";
			}
		}
		else
		{
			infoTA.PlayReverse();
			spriteTA.PlayReverse();
		}
	}
	
}
