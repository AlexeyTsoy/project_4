using UnityEngine;
using System.Collections;

public class StatsScript : MonoBehaviour 
{

	public UIPlayTween pT;

	public UILabel mainLabel;
	public UILabel swordElemDmg;
	public UILabel magicElemDmg;
	public UILabel elemDefence;

	bool isClicked;

	public void StatsButton()
	{
		mainLabel.text =
		"Armor - " + PlayerStats.instance.armor +
		"\nMagic Damage - " + PlayerStats.instance.magicDamageMin + "-" + PlayerStats.instance.magicDamageMax +
		"\nSword Damage - " + PlayerStats.instance.physicalDamageMax + "-" + PlayerStats.instance.physicalDamageMin +
		"\nCritical Chance - " + PlayerStats.instance.critChance + "%" +
		"\nDodge Chance - " + PlayerStats.instance.dodgeChance + "%";
		swordElemDmg.text = 
		"Fire Sword - " + PlayerStats.instance.physicalFireDmg +
		"\nEarth Sword - " + PlayerStats.instance.physicalEarthDmg +
		"\nWater Sword - " + PlayerStats.instance.physicalWaterDmg +
		"\nWind Sword - " + PlayerStats.instance.physicalWindDmg;
		magicElemDmg.text =
		"Fire Magic - " + PlayerStats.instance.magicFireDmg +
		"\nEarth Magic - " + PlayerStats.instance.magicEarthDmg +
		"\nWater Magic - " + PlayerStats.instance.magicWaterDmg +
		"\nWind Magic - " + PlayerStats.instance.magicWindDmg;
		elemDefence.text =
		"Fire - " + PlayerStats.instance.fireDef +
		"\nEarth - " + PlayerStats.instance.earthDef +
		"\nWater - " + PlayerStats.instance.waterDef +
		"\nWind - " + PlayerStats.instance.windDef ;
		
		if(!isClicked)
		{
			pT.Play(true);
			pT.playDirection = AnimationOrTween.Direction.Reverse;
			isClicked = true;
		}
		else 
		{
			pT.Play(true);
			pT.playDirection = AnimationOrTween.Direction.Forward;
			isClicked = false;
		}
	}
}
