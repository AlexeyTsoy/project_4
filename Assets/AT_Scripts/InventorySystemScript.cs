﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Networking;

public class InventorySystemScript : MonoBehaviour 
{
	public GameObject equipmentInventory;
	public GameObject consumableInventory;

	public ItemList list;
	public UIScrollView scrollView;

	void Awake()
	{
		ItemDataBase.DBLoad();
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.D))
		{
			foreach (var item in equipmentInventory.GetComponent<UIGrid>().GetChildList()) 
			{
				NGUITools.Destroy(item.gameObject);
			}
		}

		if(Input.GetKeyDown(KeyCode.A))
		{
			ItemQuality iQ = (ItemQuality)Random.Range(0,5);
			ItemList.instance.index = Random.Range(0, 11);
			ItemList.instance.AddEquipment(ItemList.instance.index, 60, iQ, 1, false);
			int i = ItemList.instance.equipmentItems.FindLastIndex(ItemList.instance.FindIndex);
			GameObject obj = PoolingObjectsScript.instance.GetPooledObject();
			obj.GetComponent<EquipmentScript>().index = i;
			obj.SetActive(true);
			list = ItemList.instance;
			equipmentInventory.GetComponent<UIGrid>().repositionNow = true;
			scrollView.ResetPosition();
		}

		if(Input.GetKeyDown(KeyCode.S))
		{
			ItemList.instance.index = Random.Range(0,2); 
			if(!ItemList.instance.consumableItems.Exists(ItemList.instance.FindIndex))
			{
				ItemList.instance.AddConsumable(ItemList.instance.index);
				int i = ItemList.instance.consumableItems.FindIndex(ItemList.instance.FindIndex); // не нужный код!
				GameObject obj = PoolingObjectsScript2.instance.GetPooledObject();
				obj.GetComponent<ConsumableScript>().index = i;
				obj.SetActive(true);
				list = ItemList.instance;
				consumableInventory.GetComponent<UIGrid>().repositionNow = true;
			}
			else
			{
				ItemList.instance.consumableItems[ItemList.instance.index].itemAmount ++;
			}

		}
	}

	int EqipmentSort(Transform x , Transform y)
	{
		var a = x.GetComponent<EquipmentScript>();
		var b = y.GetComponent<EquipmentScript>();
		if (b.quality == a.quality)
		{
			return b.StatsSummary().CompareTo(a.StatsSummary());
		}
		return b.quality.CompareTo(a.quality);
	}

	int ConsumableSort(Transform x , Transform y)
	{
		var a = x.GetComponent<ConsumableScript>();
		var b = y.GetComponent<ConsumableScript>();
		return b.quality.CompareTo(a.quality);
	}

}
