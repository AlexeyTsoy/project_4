﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerStats 
{
	public static PlayerStats instance = new PlayerStats();

	public int playerLevel;
	public float agility;
	public float Intellect;
	public float strength;
	public float vitality;
	public float physicalDamageMax;
	public float physicalDamageMin;
	public float magicDamageMax;
	public float magicDamageMin;
	public float physicalFireDmg;
	public float physicalEarthDmg;
	public float physicalWaterDmg;
	public float physicalWindDmg;
	public float magicFireDmg;
	public float magicEarthDmg;
	public float magicWaterDmg;
	public float magicWindDmg;
	public float dodgeChance;
	public float accuracy;
	public float armor;
	public float critChance;
	public float resilience;
	public float fireDef;
	public float earthDef;
	public float waterDef;
	public float windDef;

//	public Characteristics ()
//	{
//		позже для получения с сервера надо будет сделать формулу характеристики героя на основе PlayerLevel
//	}
}
