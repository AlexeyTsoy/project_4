﻿using UnityEngine;
using System.Collections;

public class SortedItemScript : MonoBehaviour
{
	public int index;

	public UISprite frame;
	public UISprite icon;

	public UILabel itemName;
	public UILabel mainFeature;
	public UILabel titleFeature;
	public UILabel agility;
	public UILabel intellect;
	public UILabel strength;
	public UILabel vitality;
	public UILabel itemLevel;
	public UILabel firstFeature;
	public UILabel secondFeature;

	public ItemQuality quality;

	public string itemType;

	public void Info() 
	{
		icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.equipmentItems[index].itemAtlas];

		quality = ItemList.instance.equipmentItems[index].itemQuality;
		icon.spriteName = ItemList.instance.equipmentItems[index].itemIcon;
		frame.spriteName = ItemList.instance.equipmentItems[index].itemQuality.ToString();
		itemName.text = "" + ItemList.instance.equipmentItems[index].itemName;
		mainFeature.text = "" + ItemList.instance.equipmentItems[index].itemMainFeature;
		titleFeature.text = "" + ItemList.instance.equipmentItems[index].itemTitleFeature;
		agility.text = "Agi: " + ItemList.instance.equipmentItems[index].itemAgility;
		intellect.text = "Int: " + ItemList.instance.equipmentItems[index].itemIntellect;
		strength.text = "Str: " + ItemList.instance.equipmentItems[index].itemStrenght;
		vitality.text = "Vit: " + ItemList.instance.equipmentItems[index].itemVitality;
		itemLevel.text = ItemList.instance.equipmentItems[index].itemLevel.ToString();
		firstFeature.text = "" + ItemList.instance.equipmentItems[index].skill;
		secondFeature.text = "" + ItemList.instance.equipmentItems[index].critChance;
		itemType = ItemList.instance.equipmentItems[index].itemType.ToString();
	}

	public int StatsSummary()
	{
		return ItemList.instance.equipmentItems[index].itemAgility + 
			ItemList.instance.equipmentItems[index].itemIntellect + 
			ItemList.instance.equipmentItems[index].itemStrenght +
			ItemList.instance.equipmentItems[index].itemVitality;
	}

	public void Equip()
	{
		switch(ItemList.instance.equipmentItems[index].itemType)
		{
		case ItemType.Head:
			if(CharacterManagerScript.isHeadEquipped)
			{
				isTrue(ref CharacterManagerScript.headIndex, 0);
			}
			else
			{
				isFalse(ref CharacterManagerScript.headIndex, 0);
			}
			break;
		case ItemType.Shoulder:
			if(CharacterManagerScript.isShoulderEquipped)
			{
				isTrue(ref CharacterManagerScript.shoulderIndex, 1);
			}
			else
			{
				isFalse(ref CharacterManagerScript.shoulderIndex, 1);
			}
			break;
		case ItemType.Gloves:
			if(CharacterManagerScript.isGlovesEquipped)
			{
				isTrue(ref CharacterManagerScript.glovesIndex, 2);
			}
			else
			{
				isFalse(ref CharacterManagerScript.glovesIndex, 2);
			}
			break;
		case ItemType.Boots:
			if(CharacterManagerScript.isBootsEquipped)
			{
				isTrue(ref CharacterManagerScript.bootsIndex, 3);
			}
			else
			{
				isFalse(ref CharacterManagerScript.bootsIndex, 3);
			}
			break;
		case ItemType.Necklace:
			if(CharacterManagerScript.isNecklaceEquipped)
			{
				isTrue(ref CharacterManagerScript.necklaceIndex, 4);
			}
			else
			{
				isFalse(ref CharacterManagerScript.necklaceIndex, 4);
			}
			break;
		case ItemType.Armor:
			if(CharacterManagerScript.isArmorEquipped)
			{
				isTrue(ref CharacterManagerScript.armorIndex, 5);
			}
			else
			{
				isFalse(ref CharacterManagerScript.armorIndex, 5);
			}
			break;
		case ItemType.Belt:
			if(CharacterManagerScript.isBeltEquipped)
			{
				isTrue(ref CharacterManagerScript.beltIndex, 6);
			}
			else
			{
				isFalse(ref CharacterManagerScript.beltIndex, 6);
			}
			break;
		case ItemType.Leggings:
			if(CharacterManagerScript.isLeggingsEquipped)
			{
				isTrue(ref CharacterManagerScript.leggingsIndex, 7);
			}
			else
			{
				isFalse(ref CharacterManagerScript.leggingsIndex, 7);
			}
			break;
		case ItemType.Relic:
			if(CharacterManagerScript.isRelicEquipped)
			{
				isTrue(ref CharacterManagerScript.relicIndex, 9);
			}
			else
			{
				isFalse(ref CharacterManagerScript.relicIndex, 9);
			}
			break;
		case ItemType.MainHand:
			if(CharacterManagerScript.isMainHandEquipped)
			{
				isTrue(ref CharacterManagerScript.mainHandIndex, 8);
			}
			else
			{
				isFalse(ref CharacterManagerScript.mainHandIndex, 8);

			}
			break;
		case ItemType.OffHand:
			if(CharacterManagerScript.isOffHandEquipped)
			{
				isTrue(ref CharacterManagerScript.offHandIndex, 10);
			}
			else
			{
				isFalse(ref CharacterManagerScript.offHandIndex, 10);
			}
			break;
		}

	}

	public void isTrue(ref int slot, int i)
	{
		ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[slot]); // передаем элемент из листа onCharacter в лист equipmentItems
		CharacterManagerScript.instance.Minus(slot); // минус статы
		ItemList.instance.onCharacter.RemoveAt(slot); // удаляем его из листа onCharacter
		ItemList.instance.onCharacter.Add(ItemList.instance.equipmentItems[index]); // передаем новый элемент из листа equipmentItems в лист onCharacter
		ItemList.instance.index = ItemList.instance.equipmentItems[index].itemID; // передаем индекс данного элемента для поиска в листе onCharacter
		slot = ItemList.instance.onCharacter.FindLastIndex(ItemList.instance.FindIndex);
		ItemList.instance.equipmentItems.RemoveAt(index);
		InventoryIPScript.instance.info.PlayReverse();
		InventoryIPScript.instance.sortedContent.PlayReverse();
		CharacterManagerScript.instance.Destroy();
		switch(i)
		{
		case 0:
			CharacterManagerScript.instance.HeadEquip();
			break;
		case 1:
			CharacterManagerScript.instance.ShoulderEquip();
			break;
		case 2:
			CharacterManagerScript.instance.GlovesEquip();
			break;
		case 3:
			CharacterManagerScript.instance.BootsEquip();
			break;
		case 4:
			CharacterManagerScript.instance.NecklaceEquip();
			break;
		case 5:
			CharacterManagerScript.instance.ArmorEquip();
			break;
		case 6:
			CharacterManagerScript.instance.BeltEquip();
			break;
		case 7:
			CharacterManagerScript.instance.LeggingsEquip();
			break;
		case 8:
			CharacterManagerScript.instance.MainHandEquip();
			break;
		case 9:
			CharacterManagerScript.instance.RelicEquip();
			break;
		case 10:
			CharacterManagerScript.instance.OffhandEquip();
			break;
		}
		CharacterManagerScript.instance.Plus(slot);
	}

	public void isFalse(ref int slot, int i)
	{
		ItemList.instance.onCharacter.Add(ItemList.instance.equipmentItems[index]); // добавляем из листа equipmentItems элемент в лист onCharacter
		ItemList.instance.index = ItemList.instance.equipmentItems[index].itemID; // передаем индекс данного элемента для поиска в листе onCharacter
		slot = ItemList.instance.onCharacter.FindLastIndex(ItemList.instance.FindIndex); // поиск нового индекса который уже в листе onCharacter и передем во временну переменную slot
		ItemList.instance.equipmentItems.RemoveAt(index);
		InventoryIPScript.instance.info.PlayReverse();
		InventoryIPScript.instance.sortedContent.PlayReverse();
		CharacterManagerScript.instance.Destroy();
		switch(i)
		{
		case 0:
			CharacterManagerScript.isHeadEquipped = true;
			CharacterManagerScript.instance.HeadEquip();
			break;
		case 1:
			CharacterManagerScript.isShoulderEquipped = true;
			CharacterManagerScript.instance.ShoulderEquip();
			break;
		case 2:
			CharacterManagerScript.isGlovesEquipped = true;
			CharacterManagerScript.instance.GlovesEquip();
			break;
		case 3:
			CharacterManagerScript.isBootsEquipped = true;
			CharacterManagerScript.instance.BootsEquip();
			break;
		case 4:
			CharacterManagerScript.isNecklaceEquipped = true;
			CharacterManagerScript.instance.NecklaceEquip();
			break;
		case 5:
			CharacterManagerScript.isArmorEquipped = true;
			CharacterManagerScript.instance.ArmorEquip();
			break;
		case 6:
			CharacterManagerScript.isBeltEquipped = true;
			CharacterManagerScript.instance.BeltEquip();
			break;
		case 7:
			CharacterManagerScript.isLeggingsEquipped = true;
			CharacterManagerScript.instance.LeggingsEquip();
			break;
		case 8:
			CharacterManagerScript.isMainHandEquipped = true;
			CharacterManagerScript.instance.MainHandEquip();
			break;
		case 9:
			CharacterManagerScript.isRelicEquipped = true;
			CharacterManagerScript.instance.RelicEquip();
			break;
		case 10:
			CharacterManagerScript.isOffHandEquipped = true;
			CharacterManagerScript.instance.OffhandEquip();
			break;
		}
		CharacterManagerScript.instance.Plus(slot);
	}
}
