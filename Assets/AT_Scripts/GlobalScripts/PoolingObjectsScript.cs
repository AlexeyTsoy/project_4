﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//В этом скрипте мы реализуем хранение объектов которые будут использованы для частого удаления и создания на сцене
public class PoolingObjectsScript : MonoBehaviour
{
	public static PoolingObjectsScript instance;

	public int amountOfObjects = 10; // кол-во хранимых объектов

	public bool willGrow; // если вдруг не хватит кол-во amountOfObjects

	public GameObject parent;
	public GameObject prefab; // объект который будет храниться в пуле

	public List<GameObject> pooledObjects = new List<GameObject>(); // лист для хранения объектов

	void Awake()
	{
		instance = this;
	}

	void Start ()
	{
		for (int i = 0; i < amountOfObjects; i++) 
		{
			GameObject obj = parent.AddChild(prefab);
//			GameObject obj = (GameObject)Instantiate(prefab); // без NGUI
			obj.SetActive(false);
			pooledObjects.Add(obj);
		}
	}
	
	public GameObject GetPooledObject()
	{
		for (int i = 0; i < pooledObjects.Count; i++)
		{
			if(!pooledObjects[i].activeInHierarchy)
			{
				return pooledObjects[i];
			}
		}

		if(willGrow)
		{
			GameObject obj = gameObject.AddChild(prefab); // для NGUI
//			GameObject obj = (GameObject)Instantiate(prefab);
			pooledObjects.Add(obj);
			return obj;
		}
		return null;
	}
}
