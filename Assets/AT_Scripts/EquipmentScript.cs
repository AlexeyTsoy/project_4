﻿using UnityEngine;
using System.Collections;

public class EquipmentScript : MonoBehaviour 
{
	public int index;

	public UISprite frame;
	public UISprite icon;

	public ItemQuality quality; // для сортировки

	void OnEnable() 
	{
		if(FirstContentScript.instance != null)
		{
			if(FirstContentScript.instance.equipmentInventory.GetChildList().Count > 0)
			{
				InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.equipmentItems[index].itemAtlas];
				icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.equipmentItems[index].itemAtlas];
			
				frame.spriteName = ItemList.instance.equipmentItems[index].itemQuality.ToString();
				icon.spriteName = ItemList.instance.equipmentItems[index].itemName;
				quality = ItemList.instance.equipmentItems[index].itemQuality;
			}
		}
	}

	public void Info() 
	{
		InventoryIPScript.instance.info.PlayForward();
		InventoryIPScript.instance.fade.PlayForward();

		InventoryIPScript.instance.icon.spriteName = ItemList.instance.equipmentItems[index].itemIcon;
		InventoryIPScript.instance.frame.spriteName = ItemList.instance.equipmentItems[index].itemQuality.ToString();
		InventoryIPScript.instance.itemName.text = "" + ItemList.instance.equipmentItems[index].itemName;
		InventoryIPScript.instance.quality.text = "" + ItemList.instance.equipmentItems[index].itemQuality;
		InventoryIPScript.instance.mainFeature.text = "" + ItemList.instance.equipmentItems[index].itemMainFeature;
		InventoryIPScript.instance.titleFeature.text = "" + ItemList.instance.equipmentItems[index].itemTitleFeature;
		InventoryIPScript.instance.agility.text = "Agi: " + ItemList.instance.equipmentItems[index].itemAgility;
		InventoryIPScript.instance.intellect.text = "Int: " + ItemList.instance.equipmentItems[index].itemIntellect;
		InventoryIPScript.instance.strength.text = "Str: " + ItemList.instance.equipmentItems[index].itemStrenght;
		InventoryIPScript.instance.vitality.text = "Vit: " + ItemList.instance.equipmentItems[index].itemVitality;
		InventoryIPScript.instance.itemType.text = ItemList.instance.equipmentItems[index].itemType.ToString();
		InventoryIPScript.instance.itemLevel.text = ItemList.instance.equipmentItems[index].itemLevel.ToString();
		InventoryIPScript.instance.firstFeature.text = "" + ItemList.instance.equipmentItems[index].skill;
		InventoryIPScript.instance.secondFeature.text = "" + ItemList.instance.equipmentItems[index].critChance;
	}

	public int StatsSummary()
	{
		return ItemList.instance.equipmentItems[index].itemAgility + 
			ItemList.instance.equipmentItems[index].itemIntellect + 
			ItemList.instance.equipmentItems[index].itemStrenght +
			ItemList.instance.equipmentItems[index].itemVitality;
	}
}
