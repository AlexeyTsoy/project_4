﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ElementType
{
	Earth,
	Fire,
	Water,
	Wind
}

public delegate void actionClick();

public class Global 
{
	public static ElementType ElementDamageType;
	public static ElementType magicElementDamageType;
	public static ElementType heroDefenceType;

	public static int armorMaterial;
	public static int sWeaponMaterial;
	public static int mWeaponMaterial;
	public static int fireElement;
	public static int earthElement;
	public static int waterElement;
	public static int windElement;
	public static int gold;
	public static float currentElemntalDmg;
	public static float currentMagicElemntalDmg;
	public static float currentElementalDef;

	public static actionClick invUpdate;
	public static actionClick selectedFireSkill;
	public static actionClick selectedWindSkill;
	public static actionClick selectedEarthSkill;
	public static actionClick selectedWaterSkill;

	public static actionClick singleAttack;
	public static actionClick aoeAttack;
	public static actionClick fireDOT;
	public static actionClick AOEfireDOT;
	public static actionClick wind;

	public static void EventMethod(Vector3 transformPos, GameObject parent, string eventName, int attackType)
	{
		GameObject prefab = Resources.Load("Prefabs/Event") as GameObject;
		GameObject eventPrefab = parent.AddChild(prefab);
		eventPrefab.GetComponent<UILabel>().text = eventName;
		switch(attackType)
		{
		case 0: // miss
			eventPrefab.GetComponent<UILabel>().color = Color.white;
			eventPrefab.GetComponent<UILabel>().fontSize = 45;
			eventPrefab.GetComponent<TweenPosition>().from = transformPos;
			eventPrefab.GetComponent<TweenPosition>().to = new Vector3(0, transformPos.y + 60, 0);
			eventPrefab.transform.localPosition = transformPos;
			break;
		case 1: // simple attack
			eventPrefab.GetComponent<UILabel>().color = Color.grey;
			eventPrefab.GetComponent<UILabel>().fontSize = 45;
			eventPrefab.GetComponent<TweenPosition>().from = transformPos;
			eventPrefab.GetComponent<TweenPosition>().to = new Vector3(0, transformPos.y + 60, 0);
			eventPrefab.transform.localPosition = transformPos;
			break;
		case 2: // critical strike
			eventPrefab.GetComponent<UILabel>().color = Color.red;
			eventPrefab.GetComponent<UILabel>().fontSize = 60;
			eventPrefab.GetComponent<TweenPosition>().from = transformPos;
			eventPrefab.GetComponent<TweenPosition>().to = new Vector3(0, transformPos.y + 60, 0);
			break;
		case 3: // heal
			eventPrefab.GetComponent<UILabel>().color = Color.green;
			eventPrefab.GetComponent<UILabel>().fontSize = 50;
			eventPrefab.GetComponent<TweenPosition>().from = new Vector3(transformPos.x - 40, transformPos.y, 0);
			eventPrefab.GetComponent<TweenPosition>().to = new Vector3(transformPos.x - 40, transformPos.y + 60, 0);
			break;
		}
		GameObject.Destroy(eventPrefab, 1.5f);
	}

	public static void SpellStatusCreate(GameObject parent, int counter, string prefabName, Color color)
	{
		GameObject prefab = Resources.Load("Prefabs/SpellStatus") as GameObject;
		GameObject statusPrefab = parent.AddChild(prefab);
		statusPrefab.GetComponent<UISprite>().color = color;
		statusPrefab.GetComponentInChildren<UILabel>().text = counter.ToString();
		statusPrefab.name = prefabName;
	}


}