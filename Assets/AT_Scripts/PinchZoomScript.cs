﻿using UnityEngine;

public class PinchZoomScript : MonoBehaviour
{
	public float orthoZoomSpeed;        // The rate of change of the orthographic size in orthographic mode.
	public float minZoom;
	public float maxZoom;
	public float inertialDuration = 1f;
	public float minimumScrollVelocity = 100f;
	public float mapWidth;
	public float mapHeight;

	float scrollVelocity;
	float timeTouchPhaseEnded;
	float horizontalExtent;
	float verticalExtent;
	float minX, maxX, minY, maxY;

	bool stopMove;

	Vector2 scrollDirection = Vector2.zero;

	Camera viewCam;

	void Start()
	{
		viewCam = GetComponent<Camera>();
		maxZoom = 0.5f * (mapWidth / viewCam.aspect);

		if(mapWidth > mapHeight)
		{
			maxZoom = 0.5f * mapHeight;
		}

		CalculateMapBounds();
	}

	void Update()
	{
		Ray ray = viewCam.ScreenPointToRay(new Vector2(350, 200));
		RaycastHit hit;
		Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);

		if(Physics.Raycast(ray, out hit, 100f))
		{
			if(hit.collider.name == "Cabin")
			{
				if(viewCam.orthographicSize <= 0.3f)
				{
					viewCam.transform.position = Vector2.MoveTowards(viewCam.transform.position, hit.collider.transform.position, 2f * Time.deltaTime);
					viewCam.orthographicSize = Mathf.Lerp(viewCam.orthographicSize, minZoom, 5f * Time.deltaTime);
				
					if(minZoom - viewCam.orthographicSize <= -0.02f)
					{
						viewCam.orthographicSize = minZoom;
					}
					stopMove = true;
				}
			}
		}

		if(viewCam.orthographicSize > 0.3f)
		{
			stopMove = false;
		}

		if(Input.touchCount < 1)
		{
			if(scrollVelocity != 0f)
			{
				float timer = (Time.time - timeTouchPhaseEnded) / inertialDuration;
				float frameVelocity = Mathf.Lerp(scrollVelocity, 0f, timer);
				viewCam.transform.position += -(Vector3)scrollDirection * (frameVelocity * 0.0005f) * Time.deltaTime;
				if(timer >= 1f)
				{
					scrollVelocity = 0f;
				}
			}
		}

		if(Input.touchCount == 1 && !stopMove)
		{
			Touch touchZero = Input.GetTouch(0);

			if(touchZero.phase == TouchPhase.Began)
			{
				scrollVelocity = 0f;
			}
			else if(touchZero.phase == TouchPhase.Moved)
			{
				Vector2	delta = touchZero.deltaPosition;
				float posX = (delta.x * orthoZoomSpeed) * -1;
				float posY = (delta.y * orthoZoomSpeed) * -1;

				viewCam.transform.position += new Vector3(posX, posY, 0);

				scrollDirection = touchZero.deltaPosition.normalized;

				if(touchZero.deltaTime != 0)
				{
					scrollVelocity = touchZero.deltaPosition.magnitude / touchZero.deltaTime;
				}
				else 
				{
					scrollVelocity = 0f;
				}

				if(scrollVelocity <= minimumScrollVelocity)
				{
					scrollVelocity = 0f;
				}

			}
			else if(touchZero.phase == TouchPhase.Ended)
			{
				timeTouchPhaseEnded = Time.time;
			}
		}

		if (Input.touchCount == 2)
		{
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			Vector2 cameraViewsize = new Vector2(viewCam.pixelWidth, viewCam.pixelHeight);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			viewCam.transform.position += viewCam.transform.TransformDirection((touchZeroPrevPos + touchOnePrevPos - cameraViewsize) * viewCam.orthographicSize / cameraViewsize.y);
		
			// ... change the orthographic size based on the change in distance between the touches.
			viewCam.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

			viewCam.orthographicSize = Mathf.Clamp(viewCam.orthographicSize, minZoom, maxZoom);

			viewCam.transform.position -= viewCam.transform.TransformDirection((touchZero.position + touchOne.position - cameraViewsize) * viewCam.orthographicSize / cameraViewsize.y);
		
			CalculateMapBounds();
		}
	}

	void CalculateMapBounds()
	{
		verticalExtent = viewCam.orthographicSize;
		horizontalExtent = verticalExtent * viewCam.aspect;
		minX = horizontalExtent - mapWidth / 2f;
		maxX = mapWidth / 2f - horizontalExtent;
		minY = verticalExtent - mapHeight / 2f;
		maxY = mapHeight / 2f - verticalExtent;
	}

	void LateUpdate()
	{
		Vector3 limitedCamPos = viewCam.transform.position;
		limitedCamPos.x = Mathf.Clamp(limitedCamPos.x, minX , maxX);
		limitedCamPos.y = Mathf.Clamp(limitedCamPos.y, minY, maxY);
		viewCam.transform.position = limitedCamPos;
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireCube(Vector3.zero, new Vector3(mapWidth, mapHeight, 0));
	}
}