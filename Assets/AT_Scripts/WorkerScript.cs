﻿using UnityEngine;
using System.Collections;

public class WorkerScript : MonoBehaviour 
{
	public Vector2 posA, posB;

	bool isMove = true;

	void Start () 
	{
	
	}
	
	void Update () 
	{
		if(isMove)
		{
			transform.localPosition = Vector2.MoveTowards(transform.localPosition, posA, 30f * Time.deltaTime);
			if(transform.localPosition.x - posA.x >= -1f)
			{
				isMove = false;
			}
		}
		else
		{
			transform.localPosition = Vector2.MoveTowards(transform.localPosition, posB, 30f * Time.deltaTime);
			if(transform.localPosition.x - posB.x <= 1f)
			{
				isMove = true;
			}
		}
	}
}
