﻿using UnityEngine;
using System.Collections;

public class ConsumableScript : MonoBehaviour 
{
	public int index;

	public UISprite frame;
	public UISprite icon;

	public UILabel amount;

	public ItemQuality quality; // для сортировки

	void OnEnable() 
	{
		if(SecondContentScript.instance != null)
		{
			if(SecondContentScript.instance.consumableInventory.GetChildList().Count > 0)
			{
				InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.consumableItems[index].itemAtlas];
				icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.consumableItems[index].itemAtlas];

				frame.spriteName = ItemList.instance.consumableItems[index].itemQuality.ToString();
				icon.spriteName = ItemList.instance.consumableItems[index].itemName;
				amount.text = ItemList.instance.consumableItems[index].itemAmount.ToString();
				quality = ItemList.instance.consumableItems[index].itemQuality;
			}
		}
	}

	public void Info() 
	{
		InventoryIPScript.instance.info.PlayForward();
		InventoryIPScript.instance.fade.PlayForward();
		amount.text = ItemList.instance.consumableItems[index].itemAmount.ToString();

		InventoryIPScript.instance.icon.spriteName = ItemList.instance.consumableItems[index].itemIcon;
		InventoryIPScript.instance.frame.spriteName = ItemList.instance.consumableItems[index].itemQuality.ToString();
		InventoryIPScript.instance.amount.text = ItemList.instance.consumableItems[index].itemAmount.ToString();
		InventoryIPScript.instance.itemName.text = "" + ItemList.instance.consumableItems[index].itemName;
		InventoryIPScript.instance.quality.text = "" + ItemList.instance.consumableItems[index].itemQuality;
		InventoryIPScript.instance.mainFeature.text = "" + ItemList.instance.consumableItems[index].itemMainFeature;
		InventoryIPScript.instance.titleFeature.text = "" + ItemList.instance.consumableItems[index].itemTitleFeature;
		InventoryIPScript.instance.agility.text = "Agi: " + ItemList.instance.consumableItems[index].itemAgility;
		InventoryIPScript.instance.intellect.text = "Int: " + ItemList.instance.consumableItems[index].itemIntellect;
		InventoryIPScript.instance.strength.text = "Str: " + ItemList.instance.consumableItems[index].itemStrenght;
		InventoryIPScript.instance.vitality.text = "Vit: " + ItemList.instance.consumableItems[index].itemVitality;
		InventoryIPScript.instance.itemType.text = ItemList.instance.consumableItems[index].itemType.ToString();
		InventoryIPScript.instance.itemLevel.text = ItemList.instance.consumableItems[index].itemLevel.ToString();
		InventoryIPScript.instance.firstFeature.text = "" + ItemList.instance.consumableItems[index].skill;
		InventoryIPScript.instance.secondFeature.text = "" + ItemList.instance.consumableItems[index].critChance;
	}
}
