﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum RoomType
{
	Empty,
	Kitchen,
	Garden,
	Boiler,
	Mine,
	Forge,
	Barack,
	Library,
	Trader,
	Enchantment
}

public class Ship
{
	public static RoomType room0;
	public static RoomType room1;
	public static RoomType room2;
	public static RoomType room3;
	public static RoomType room4;
	public static RoomType room5;
	public static RoomType room6;
	public static RoomType room7;
	public static RoomType room8;

	public static bool isFirstCabinBought;
	public static bool isSecondCabinBought;
	public static bool isThirdCabinBought;
	public static bool isFourthCabinBought;
	public static bool isFifthCabinBought;
	public static bool isSixthCabinBought;
	public static bool isSeventhCabinBought;
	public static bool isEighthCabinBought;
	public static bool isNinthCabinBought;

	public static int kitchenLevel;
	public static int gardenLevel;
	public static int boilerLevel;
	public static int mineLevel;
	public static int forgeLevel;
	public static int barackLevel;
	public static int libraryLevel;
	public static int traderLevel;
	public static int enchantmentLevel;

	public static float priceKitchen;
	public static float priceGarden;
	public static float priceBoiler;
	public static float priceMine;
	public static float priceForge;
	public static float priceBarack;
	public static float priceLibrary;
	public static float priceTrader;
	public static float priceEnchantment;

	public static float priceKitchenWorker;
	public static float priceGardenWorker;
	public static float priceBoilerWorker;
	public static float priceMineWorker;
	public static float priceForgeWorker;
	public static float priceBarackWorker;
	public static float priceLibraryWorker;
	public static float priceTraderWorker;
	public static float priceEnchantemntWorker;

	public static float food;
	public static float products;
	public static float energy;
	public static float minerals;
	public static float equipemnts;
	public static float warriors;
	public static float letters;
	public static float money;
	public static float enchantment;
}

public class Worker
{
	public string name;

	public int workerLevel;

	public Worker(string name, int workerLevel)
	{
		this.name = name;
		this.workerLevel = workerLevel;
	}
}

public class WorkerList
{
	public List<Worker> kitchenWorkers = new List<Worker>();
	public List<Worker> gardenWorkers = new List<Worker>();
	public List<Worker> boilerWorkers = new List<Worker>();
	public List<Worker> mineWorkers = new List<Worker>();
	public List<Worker> forgeWorkers = new List<Worker>();
	public List<Worker> barackWorkers = new List<Worker>();
	public List<Worker> libraryWorkers = new List<Worker>();
	public List<Worker> tradeWorkers = new List<Worker>();
	public List<Worker> enchantmentWorkers = new List<Worker>();

	public static WorkerList instance = new WorkerList();

	public void AddKitchenWorker(string name, int workerLevel)
	{
		kitchenWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddGardenWorker(string name, int workerLevel)
	{
		gardenWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddBoilerWorker(string name, int workerLevel)
	{
		boilerWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddMineWorker(string name, int workerLevel)
	{
		mineWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddForgeWorker(string name, int workerLevel)
	{
		forgeWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddBarackWorker(string name, int workerLevel)
	{
		barackWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddLibraryWorker(string name, int workerLevel)
	{
		libraryWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddTradeWorker(string name, int workerLevel)
	{
		tradeWorkers.Add(new Worker(name, workerLevel));
	}

	public void AddEnchantmentWorker(string name, int workerLevel)
	{
		enchantmentWorkers.Add(new Worker(name, workerLevel));
	}
}
