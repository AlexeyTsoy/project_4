﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour 
{
	void Start()
	{
		SceneManager.LoadScene("Skills", LoadSceneMode.Additive);
	}

	public void HomeScene()
	{
		if(!SceneManager.GetSceneByName("Ship").isLoaded)
		{
			SceneManager.LoadScene("Ship", LoadSceneMode.Additive);
			if(SceneManager.sceneCount > 2)
			{
				Unload();
			}
		}
	}

	public void EquipScene()
	{
		if(!SceneManager.GetSceneByName("Character").isLoaded)
		{
			SceneManager.LoadScene("Character", LoadSceneMode.Additive);
			if(SceneManager.sceneCount > 2)
			{
				Unload();
			}
		}
	}

	public void CharScene()
	{
		SceneManager.LoadScene(1, LoadSceneMode.Additive);
	}

	public void InventoryScene()
	{
		if(!SceneManager.GetSceneByName("Inventory").isLoaded)
		{
			SceneManager.LoadScene("Inventory", LoadSceneMode.Additive);
			if(SceneManager.sceneCount > 2)
			{
				Unload();
			}
		}
	}

	public void SkillsScene()
	{
		if(!SceneManager.GetSceneByName("Skills").isLoaded)
		{
			SceneManager.LoadScene("Skills", LoadSceneMode.Additive);
			if(SceneManager.sceneCount > 2)
			{
				Unload();
			}
		}
	}

	public void MapScene()
	{
		if(!SceneManager.GetSceneByName("WorldMap").isLoaded)
		{
			SceneManager.LoadScene("WorldMap", LoadSceneMode.Additive);
			if(SceneManager.sceneCount > 2)
			{
				Unload();
			}
		}
	}

	public void Unload()
	{
		string unload = SceneManager.GetSceneAt(1).name;
		SceneManager.UnloadScene(unload);
	}

}
