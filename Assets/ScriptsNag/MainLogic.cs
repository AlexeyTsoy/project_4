﻿using UnityEngine;
using System.Collections;

public class MainLogic : MonoBehaviour {
    public int coins;
    public int resLowQ;
    public int resMediumQ;
    public int resHighQ;
    public int cityHallLvl;
    public UISprite cityHallSprite;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void CityHallUpgrade()
    {
        switch (cityHallLvl)
        {
            case 0:
                if(resLowQ >= 50 && coins >= 100)
                {
                    cityHallLvl = 1;
                    cityHallSprite.spriteName = "CityHall1";
                }
                break;
            case 1:
                if (resLowQ >= 50 && coins >= 100)
                {
                    cityHallLvl = 2;
                    cityHallSprite.spriteName = "CityHall2";
                }
                break;
            case 2:
                if (resLowQ >= 50 && coins >= 100)
                {
                    cityHallLvl = 3;
                    cityHallSprite.spriteName = "CityHall3";
                }
                break;
            case 3:
                if (resLowQ >= 50 && coins >= 100)
                {
                    cityHallLvl = 4;
                    cityHallSprite.spriteName = "CityHall4";
                }
                break;
            case 4:
                if (resLowQ >= 50 && coins >= 100)
                {
                    cityHallLvl = 5;
                    cityHallSprite.spriteName = "CityHall5";
                }
                break;
           

        }
    }
}
