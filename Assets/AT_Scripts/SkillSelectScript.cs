﻿using UnityEngine;
using System.Collections;

public class SkillSelectScript : MonoBehaviour 
{
	public UIToggle checkbox;

	void OnEnable()
	{
		switch (checkbox.group) 
		{
		case 2:
			Global.selectedFireSkill += SelectFireSkill;
			break;
		case 3:
			Global.selectedWindSkill += SelectWindSkill;
			break;
		case 4:
			Global.selectedEarthSkill += SelectEarthSkill;
			break;
		case 5:
			Global.selectedWaterSkill += SelectWaterSkill;
			break;
		}
	}

	void OnDisable()
	{
		Global.selectedFireSkill -= SelectFireSkill;
		Global.selectedWindSkill -= SelectWindSkill;
		Global.selectedEarthSkill -= SelectEarthSkill;
		Global.selectedWaterSkill -= SelectWaterSkill;
	}

	void Start() // при старте определяется какие способности уже выбраны
	{
		switch (gameObject.name)
		{
		case "Burn":
			checkbox.value = Skills.isFireDOTselected;
			break;
		case "Whip":
			checkbox.value = Skills.isExtraCritChanceSelected;
			break;
		}
	}

	public void Check() // проверка какую из групп нужно использовать для проверки выбранных способностей
	{
		switch (checkbox.group) 
		{
		case 2:
			if(Global.selectedFireSkill != null)
			{
				Global.selectedFireSkill();
			}
			break;
		case 3:
			if(Global.selectedWindSkill != null)
			{
				Global.selectedWindSkill();
			}
			break;
		}
	}

	public void SelectFireSkill()
	{
		Skills.isFireDOTselected = checkbox.value;
		print("dot" + Skills.isFireDOTselected);
	}

	public void SelectWindSkill()
	{
		Skills.isExtraCritChanceSelected = checkbox.value;
		print("crit" + Skills.isExtraCritChanceSelected);
	}

	public void SelectEarthSkill()
	{
		
	}

	public void SelectWaterSkill()
	{

	}
		
}
