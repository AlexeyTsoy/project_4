﻿using UnityEngine;
using System.Collections;

public class SecondContentScript : MonoBehaviour
{
	public UIGrid consumableInventory;

	public static SecondContentScript instance;

	void Awake()
	{
		instance = this;
	}

	void OnEnable()
	{
		if(ItemList.instance.consumableItems.Count > 0)
		{
			foreach (var item in consumableInventory.GetChildList()) 
			{
				NGUITools.SetActive(item.gameObject, false);
			}

			for (int i = 0; i < ItemList.instance.consumableItems.Count; i++)
			{
				GameObject obj = PoolingObjectsScript2.instance.GetPooledObject();
				obj.GetComponent<ConsumableScript>().index = i;
				obj.SetActive(true);
			}

			consumableInventory.repositionNow = true;
		}
	}

}
