﻿using UnityEngine;
using System.Collections;

public class InventoryIPScript : MonoBehaviour
{
	public static InventoryIPScript instance;

	public TweenAlpha info;
	public TweenAlpha fade;
	public TweenAlpha itemContent;
	public TweenAlpha sortedContent;

	public UILabel itemName;
	public UILabel quality;
	public UILabel mainFeature;
	public UILabel titleFeature;
	public UILabel agility;
	public UILabel intellect;
	public UILabel strength;
	public UILabel vitality;
	public UILabel itemType;
	public UILabel itemLevel;
	public UILabel firstFeature;
	public UILabel secondFeature;
	public UILabel amount;

	public UISprite icon;
	public UISprite frame;

	public UIAtlas[] atlas;

	void Awake() 
	{
		instance = this;
	}

	public void Upgrade(int parameter)
	{
		parameter ++;
	}

	public void Reforging()
	{
		
	}

	public void Sell()
	{
		
	}

	public void Unequip()
	{
		switch(CharacterManagerScript.instance.title)
		{
		case "Head":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.headIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.headIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.headIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isHeadEquipped = false;
			CharacterManagerScript.instance.HeadEquip();
			break;
		case "Shoulder":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.shoulderIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.shoulderIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.shoulderIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isShoulderEquipped = false;
			CharacterManagerScript.instance.ShoulderEquip();
			break;
		case "Gloves":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.glovesIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.glovesIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.glovesIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isGlovesEquipped = false;
			CharacterManagerScript.instance.GlovesEquip();
			break;
		case "Boots":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.bootsIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.bootsIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.bootsIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isBootsEquipped = false;
			CharacterManagerScript.instance.BootsEquip();
			break;
		case "Necklace":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.necklaceIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.necklaceIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.necklaceIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isNecklaceEquipped = false;
			CharacterManagerScript.instance.NecklaceEquip();
			break;
		case "Armor":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.armorIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.armorIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.armorIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isArmorEquipped = false;
			CharacterManagerScript.instance.ArmorEquip();
			break;
		case "Belt":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.beltIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.beltIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.beltIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isBeltEquipped = false;
			CharacterManagerScript.instance.BeltEquip();
			break;
		case "Leggings":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.leggingsIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.leggingsIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.leggingsIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isLeggingsEquipped = false;
			CharacterManagerScript.instance.LeggingsEquip();
			break;
		case "Relic":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.relicIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.relicIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.relicIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isRelicEquipped = false;
			CharacterManagerScript.instance.RelicEquip();
			break;
		case "Main":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.mainHandIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.mainHandIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.mainHandIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isMainHandEquipped = false;
			CharacterManagerScript.instance.MainHandEquip();
			break;
		case "Offhand":
			ItemList.instance.equipmentItems.Add(ItemList.instance.onCharacter[CharacterManagerScript.offHandIndex]); // передаем элемент из листа onCharacter в лист equipmentItems
			CharacterManagerScript.instance.Minus(CharacterManagerScript.offHandIndex);
			ItemList.instance.onCharacter.RemoveAt(CharacterManagerScript.offHandIndex); // удаляем его из листа onCharacter
			info.PlayReverse();
			itemContent.PlayReverse();
			CharacterManagerScript.isOffHandEquipped = false;
			CharacterManagerScript.instance.OffhandEquip();
			break;
		}
			
		for (int i = 0; i < ItemList.instance.onCharacter.Count; i++) 
		{
			switch(ItemList.instance.onCharacter[i].itemType)
			{
			case ItemType.Head:
				CharacterManagerScript.headIndex = i;
				break;
			case ItemType.Shoulder:
				CharacterManagerScript.shoulderIndex = i;
				break;
			case ItemType.Gloves:
				CharacterManagerScript.glovesIndex = i;
				break;
			case ItemType.MainHand:
				CharacterManagerScript.mainHandIndex = i;
				break;
			case ItemType.OffHand:
				CharacterManagerScript.offHandIndex = i;
				break;
			case ItemType.Boots:
				CharacterManagerScript.bootsIndex = i;
				break;
			case ItemType.Leggings:
				CharacterManagerScript.leggingsIndex = i;
				break;
			case ItemType.Belt:
				CharacterManagerScript.beltIndex = i;
				break;
			case ItemType.Armor:
				CharacterManagerScript.armorIndex = i;
				break;
			case ItemType.Relic:
				CharacterManagerScript.relicIndex = i;
				break;
			}
		}
	}

	public void Swap()
	{
		switch(CharacterManagerScript.instance.title)
		{
		case "Head":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Head);
			break;
		case "Shoulder":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Shoulder);
			break;
		case "Gloves":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Gloves);
			break;
		case "Boots":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Boots);
			break;
		case "Necklace":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Necklace);
			break;
		case "Armor":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Armor);
			break;
		case "Belt":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Belt);
			break;
		case "Leggings":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Leggings);
			break;
		case "Relic":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.Relic);
			break;
		case "Main":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.MainHand);
			break;
		case "Offhand":
			itemName.text = CharacterManagerScript.instance.title;
			itemContent.PlayReverse();
			sortedContent.PlayForward();
			CharacterManagerScript.instance.Sorting(ItemType.OffHand);
			break;
		}
	}
}
