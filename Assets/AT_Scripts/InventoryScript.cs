﻿using UnityEngine;
using System.Collections;

public class InventoryScript : MonoBehaviour 
{
	public UIPlayTween PT;
	public UILabel armorCounter;
	public UILabel mWeaponCounter;
	public UILabel aWeaponCounter;
	public UILabel fireElemetCounter;
	public UILabel lightingElemetCounter;
	public UILabel waterElemetCounter;
	public UILabel windElemetCounter;
	public UILabel goldCounter;
	public UILabel potionCounter;

	bool isClicked;

	void Start()
	{
		if(Global.invUpdate != null)
		{
			Global.invUpdate();
		}
	}

	void Update()
	{
	}

	void OnEnable()
	{
		Global.invUpdate += InventoryUpdate;
	}
	
	void OnDisable()
	{
		Global.invUpdate -= InventoryUpdate;
	}


	void InventoryUpdate()
	{
		armorCounter.text = "" + Global.armorMaterial;
		mWeaponCounter.text = "" + Global.sWeaponMaterial;
		aWeaponCounter.text = "" + Global.mWeaponMaterial;
		fireElemetCounter.text = "" + Global.fireElement;
		lightingElemetCounter.text = "" + Global.earthElement;
		waterElemetCounter.text = "" + Global.waterElement;
		windElemetCounter.text = "" + Global.windElement;
//		potionCounter.text = "" + GlobalScript.potionAmount;
		goldCounter.text = "Gold: " + Global.gold;
	}

	public void InventoryButton()
	{
		if(!isClicked)
		{
			PT.Play(true);
			PT.playDirection = AnimationOrTween.Direction.Reverse;
			isClicked = true;
		}
		else 
		{
			PT.Play(true);
			PT.playDirection = AnimationOrTween.Direction.Forward;
			isClicked = false;
		}
	}

}
