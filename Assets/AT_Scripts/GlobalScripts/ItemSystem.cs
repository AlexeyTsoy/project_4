﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ItemType
{
	Head,
	Shoulder,
	Gloves,
	MainHand,
	OffHand,
	Boots,
	Leggings,
	Belt,
	Armor,
	Relic,
	Necklace,
	Bottle
}

public enum ItemQuality
{
	Common,
	Uncommon,
	Rare,
	Epic,
	Legendary
}

[System.Serializable]
public class Item 
{
	public string itemName;
	public string itemIcon;
	public string itemTitleFeature;

	public float itemMainFeature;

	public int itemID;
	public int itemAtlas;
	public int itemArmor;
	public int itemAgility;
	public int itemIntellect;
	public int itemStrenght;
	public int itemVitality;
	public int itemLevel;
	public int itemAmount;
	public int critChance;
	public int skill;

	public ItemType itemType;
	public ItemQuality itemQuality;

	//конструктор для equipmentItems
	public Item(
		int itemID, int itemAtlas, int itemLevel, string itemName, 
		int itemAgility, int itemIntellegence, int itemStrenght, int itemVitality, 
		string titleFeature, float itemMainFeature, ItemType itemType, ItemQuality itemQuality, 
		int skill, bool critChance)
	{
		this.itemID = itemID;
		this.itemAtlas = itemAtlas;
		this.itemName = itemName;
		this.itemIcon = itemName;
		this.itemAgility = itemAgility;
		this.itemIntellect = itemIntellegence;
		this.itemStrenght = itemStrenght;
		this.itemVitality = itemVitality;
		this.itemLevel = itemLevel;
		this.itemTitleFeature = titleFeature;
		this.itemMainFeature = itemMainFeature;
		this.itemType = itemType;
		this.itemQuality = itemQuality;

		if(critChance)
		{
			this.critChance = Random.Range(5,10);
		}

		switch (skill)
		{
		case 1:
			skill = Random.Range(1,3);
			break;
		case 2:
			skill = Random.Range(1,3);
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		}
	}

	//конструктор для equipmentDB
	public Item(int itemID, int itemAtlas, int itemLevel, string itemName, string itemTitleFeature, float itemMainFeature, ItemType itemType)
	{
		this.itemID = itemID;
		this.itemAtlas = itemAtlas;
		this.itemName = itemName;
		this.itemLevel = itemLevel;
		this.itemTitleFeature = itemTitleFeature;
		this.itemMainFeature = itemMainFeature;
		this.itemType = itemType;
	}

	//конструктор для consumableDB и consumableItems
	public Item(int itemID, int itemAtlas, int itemLevel, int itemAmount, string itemName, string itemTitleFeature, float itemMainFeature, ItemType itemType, ItemQuality itemQuality)
	{
		this.itemID = itemID;
		this.itemAtlas = itemAtlas;
		this.itemLevel = itemLevel;
		this.itemAmount = itemAmount;
		this.itemName = itemName;
		this.itemIcon = itemName;
		this.itemTitleFeature = itemTitleFeature;
		this.itemMainFeature = itemMainFeature;
		this.itemType = itemType;
		this.itemQuality = itemQuality;
	}
}

[System.Serializable]
public class ItemList
{
	public List<Item> equipmentItems = new List<Item>();
	public List<Item> consumableItems = new List<Item>();
	public List<Item> onCharacter = new List<Item>();

	public int index;

	public static ItemList instance = new ItemList();

	public void AddEquipment(int itemID, int statPoints, ItemQuality itemQuality, int skill, bool critChance)
	{
		equipmentItems.Add(new Item(
			ItemDataBase.equipmentDB[itemID].itemID,
			ItemDataBase.equipmentDB[itemID].itemAtlas,
			ItemDataBase.equipmentDB[itemID].itemLevel,
			ItemDataBase.equipmentDB[itemID].itemName, 
			Random.Range((statPoints / 2), statPoints),
			Random.Range((statPoints / 2), statPoints),
			Random.Range((statPoints / 2), statPoints), 
			Random.Range((statPoints / 2), statPoints), 
			ItemDataBase.equipmentDB[itemID].itemTitleFeature,
			ItemDataBase.equipmentDB[itemID].itemMainFeature,
			ItemDataBase.equipmentDB[itemID].itemType,
			itemQuality, skill, critChance));
	}

	public void AddConsumable(int itemID)
	{
		consumableItems.Add(new Item(
			ItemDataBase.consumableDB[itemID].itemID,
			ItemDataBase.consumableDB[itemID].itemAtlas,
			ItemDataBase.consumableDB[itemID].itemLevel,
			ItemDataBase.consumableDB[itemID].itemAmount,
			ItemDataBase.consumableDB[itemID].itemName,
			ItemDataBase.consumableDB[itemID].itemTitleFeature,
			ItemDataBase.consumableDB[itemID].itemMainFeature,
			ItemDataBase.consumableDB[itemID].itemType,
			ItemDataBase.consumableDB[itemID].itemQuality));
	}

	public bool FindIndex(Item item) // это предикат для List'а
	{
		if (item.itemID == index)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}