﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerScript : MonoBehaviour
{
	public static PlayerScript instance;

	public static bool isCoroutineEnd = true;

	public float maxHp;

	public UICenterOnChild weapon;
	public UICenterOnChild shield;

	public UILabel hpLabel;

	public UISlider hpSlider;


	public List<actionClick> listOfSkills = new List<actionClick>();
	//public int @if; //пример испоьзования @, для зарезервированного имени оператора IF, в качестве идентификатора переменной
	UIGrid enemyBar;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		enemyBar = GameObject.Find("EnemyBar").GetComponent<UIGrid>();
	}

	void OnEnable()
	{
		weapon.onCenter += Attack;
		shield.onCenter += Shield;
	}

	void OnDisable()
	{
		weapon.onCenter -= Attack;
		shield.onCenter -= Shield;
	}

	public void SendDamage()
	{
		StartCoroutine("CoroutinSkills", 0.1f);
	}

	IEnumerator CoroutinGetDamage(float time) 
	{
		for(int i = 0; i < enemyBar.GetChildList().Count; i++) 
		{
			enemyBar.GetChild(i).GetComponent<EnemyScript>().SendDamage();
			yield return new WaitForSeconds(time);
		}
	}

	IEnumerator CoroutinSkills(float time)
	{
		listOfSkills.AddRange(new actionClick[] {Global.fireDOT, Global.AOEfireDOT, Global.wind});

		for(int i = 0; i < listOfSkills.Count; i++)
		{
			if(listOfSkills[i] != null)
			{
				listOfSkills[i]();
			}
			yield return new WaitForSeconds(time);
		}
		yield return StartCoroutine("CoroutinGetDamage", 0.4f);

		listOfSkills.Clear();
		isCoroutineEnd = true;
		print(isCoroutineEnd);
	}

	public void AOEattack()
	{
		if(isCoroutineEnd)
		{
			if(Global.aoeAttack != null)
			{
				Global.aoeAttack();
				SendDamage();
				isCoroutineEnd = false;
			}
		}
	}

	void LabelHP()
	{
		hpLabel.text = Mathf.RoundToInt(hpSlider.value * maxHp) + " HP"; // получить целое число HP 
	}

	public void ReceiveDamage(float dmg, ElementType damageType, bool isCrit)
	{
		float incomingDmg;

		if(Global.heroDefenceType == damageType) // проверка героя на тип защиты от урона врага
		{
			print ("Block!");
			incomingDmg = (dmg * (100 - PlayerStats.instance.armor) / 100 * (100 - Global.currentElementalDef) / 100);
			hpSlider.value -= incomingDmg / maxHp;
			if(isCrit)
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "!B" + (int)incomingDmg, 2);
			}
			else
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "-B" + (int)incomingDmg, 1);
			}

		}
		//проверка на увеличенный урон в случае слабости одной стихии против другой
		else if (damageType == ElementType.Fire && Global.heroDefenceType == ElementType.Wind)
		{
			incomingDmg = ((dmg * (100 - PlayerStats.instance.armor) / 100) * 1.25f);
			hpSlider.value -= incomingDmg / maxHp;
			if(isCrit)
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "!!" + (int)incomingDmg, 2);
			}
			else
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "-!" + (int)incomingDmg, 1);
			}
			print ("fire wind");
		}
		else if (damageType == ElementType.Wind && Global.heroDefenceType == ElementType.Earth)
		{
			incomingDmg = ((dmg * (100 - PlayerStats.instance.armor) / 100) * 1.25f);
			hpSlider.value -= incomingDmg / maxHp;
			if(isCrit)
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "!!" + (int)incomingDmg, 2);
			}
			else
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "-!" + (int)incomingDmg, 1);
			}
			print ("wind earth");
		}
		else if (damageType == ElementType.Earth && Global.heroDefenceType == ElementType.Water)
		{
			incomingDmg = ((dmg * (100 - PlayerStats.instance.armor) / 100) * 1.25f);
			hpSlider.value -= incomingDmg / maxHp;
			if(isCrit)
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "!!" + (int)incomingDmg, 2);
			}
			else
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "-!" + (int)incomingDmg, 1);
			}
			print ("earth water");
		}
		else if (damageType == ElementType.Water && Global.heroDefenceType == ElementType.Fire)
		{
			incomingDmg = ((dmg * (100 - PlayerStats.instance.armor) / 100) * 1.25f);
			hpSlider.value -= incomingDmg / maxHp;
			if(isCrit)
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "!!" + (int)incomingDmg, 2);
			}
			else
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "-!" + (int)incomingDmg, 1);
			}
			print ("water fire");
		}
		else
		{
			incomingDmg = (dmg * (100 - PlayerStats.instance.armor) / 100);
			hpSlider.value -= incomingDmg / maxHp;
			if(isCrit)
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "!" + (int)incomingDmg, 2);
			}
			else
			{
				Global.EventMethod(new Vector3 (0, 80, 0), gameObject, "-" + (int)incomingDmg, 1);
			}
			print ("No extra damage");
		}
	}

	void Update () 
	{
	}

	void Attack(GameObject _centeredItem)
	{
		switch(_centeredItem.name)
		{
		case "Earth":
			Global.ElementDamageType = ElementType.Earth;
			Global.currentElemntalDmg = PlayerStats.instance.physicalEarthDmg;
			print("Earth");
			break;
		case "Fire":
			Global.ElementDamageType = ElementType.Fire;
			Global.currentElemntalDmg = PlayerStats.instance.physicalFireDmg;
			print("Fire");
			break;
		case "Water":
			Global.ElementDamageType = ElementType.Water;
			Global.currentElemntalDmg = PlayerStats.instance.physicalWaterDmg;
			print("Water");
			break;
		case "Wind":
			Global.ElementDamageType = ElementType.Wind;
			Global.currentElemntalDmg = PlayerStats.instance.physicalWindDmg;
			print("Wind");
			break;
		}
	}

	void Shield(GameObject _centeredItem)
	{
		switch(_centeredItem.name)
		{
		case "EarthShield":
			Global.heroDefenceType = ElementType.Earth;
			Global.currentElementalDef = PlayerStats.instance.earthDef;
			print("EarthShield");
			break;
		case "FireShield":
			Global.heroDefenceType = ElementType.Fire;
			Global.currentElementalDef = PlayerStats.instance.fireDef;
			print("FireShield");
			break;
		case "WaterShield":
			Global.heroDefenceType = ElementType.Water;
			Global.currentElementalDef = PlayerStats.instance.waterDef;
			print("WaterShield");
			break;
		case "WindShield":
			Global.heroDefenceType = ElementType.Wind;
			Global.currentElementalDef = PlayerStats.instance.windDef;
			print("WindShield");
			break;
		}
	}

}