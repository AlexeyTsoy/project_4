﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyScript : MonoBehaviour 
{
	public GameObject statusBar;

	public UILabel hpLabel;
	public UILabel lvlLabel;

	public UISlider hpSlider;
	public UISprite iconDamageType;
	public UISprite iconWeaknessType;

	public ElementType weakness;
	public ElementType enemyDamageType;

	public float maxHp;
	public float dodgeChance;
	public float accuracy;
	public float critChance;
	public float currentDamage;
	public float defence;

	public int level;
	public int fireDOTtime;
	public int AOEfireDOTtime;
	public int critTime;

	float playerDogdeChance;
	float armor;

	bool die;
	bool enemyCriticalStrike;
	bool enemyDodge;

	SkillsScript skills;

	void Awake()
	{
		skills = GetComponent<SkillsScript>();
	}

	void Start ()
	{
		lvlLabel.text = "" + level;
		critChance = (level * 1.3f) - PlayerStats.instance.resilience;
		if(critChance <= 0)
		{
			critChance = 0;
		}

		int randomExtraDodgeChance = Random.Range(1, 100); // проверка на шанс увеличенного шанса уворота
		bool multiplierDodgeChance = randomExtraDodgeChance <= 25 ? true:false;
		if(multiplierDodgeChance)
		{
			dodgeChance = Random.Range((level * 1.5f), (level * 1.6f));
		}
		else
		{
			dodgeChance = Random.Range((level * 1f), (level * 1.5f));
		}

		dodgeChance = (dodgeChance - PlayerStats.instance.accuracy) * 2.5f;
		if(dodgeChance <= 0)
		{
			dodgeChance = 0;
		}

		int randomExtraAccuracy = Random.Range(1, 100); // проверка на шанс увеличенного шанса точности
		bool multiplierAccuracy = randomExtraAccuracy <= (level * 0.5) ? true:false;
		if(multiplierAccuracy)
		{
			accuracy = Random.Range((level * 1.5f), (level * 1.6f));
		}
		else
		{
			accuracy = Random.Range((level * 1f), (level * 1.5f));
		}
		
		playerDogdeChance = (PlayerStats.instance.dodgeChance - accuracy) * 2.5f;
		accuracy = 100 - playerDogdeChance;
		if(accuracy <= 0)
		{
			accuracy = 0;
		}

		int randomExtraArmor = Random.Range(1, 100); // проверка на шанс увеличенной брони
		bool multiplierArmor = randomExtraArmor <= 25 ? true:false;
		if(multiplierArmor)
		{
			armor = Random.Range((level * 1.5f), (level * 1.6f));
		}
		else
		{
			armor = Random.Range((level * 1f), (level * 1.5f));
		}

		defence = 100 - (int)armor;
		armor = (100 - armor) / 100;

		int randomDmgType = Random.Range(1, 5); // генерация типа стихийного урона (и для лейблов)
		switch(randomDmgType)
		{
		case 1:
			enemyDamageType = ElementType.Earth;
			break;
		case 2:
			enemyDamageType = ElementType.Fire;
			break;
		case 3:
			enemyDamageType = ElementType.Water;
			break;
		case 4:
			enemyDamageType = ElementType.Wind;
			break;
		}

		int randomWeakness = Random.Range(1, 5); // генерация типа стихийной слабости
		switch(randomWeakness)
		{
		case 1:
			weakness = ElementType.Earth;
			break;
		case 2:
			weakness = ElementType.Fire;
			break;
		case 3:
			weakness = ElementType.Water;
			break;
		case 4:
			weakness = ElementType.Wind;
			break;
		}

		switch(enemyDamageType)
		{
		case ElementType.Earth:
			iconDamageType.color = new Color32(108, 216, 131, 255);
			break;
		case ElementType.Fire:
			iconDamageType.color = new Color32(238, 117, 117, 255);
			break;
		case ElementType.Water:
			iconDamageType.color = new Color32(115, 112, 223, 255);
			break;
		case ElementType.Wind:
			iconDamageType.color = new Color32(167, 167, 167, 255);
			break;
		}
		
		switch(weakness)
		{
		case ElementType.Earth:
			iconWeaknessType.color = new Color32(108, 216, 131, 255);
			break;
		case ElementType.Fire:
			iconWeaknessType.color = new Color32(238, 117, 117, 255);
			break;
		case ElementType.Water:
			iconWeaknessType.color = new Color32(115, 112, 223, 255);
			break;
		case ElementType.Wind:
			iconWeaknessType.color = new Color32(167, 167, 167, 255);
			break;
		}
	}

	void OnEnable()
	{
		Global.aoeAttack += EnemyDodgeMethod;
		Global.aoeAttack += SkillsSelected;
	}
	
	void OnDisable()
	{
		Global.aoeAttack -= EnemyDodgeMethod;
		Global.aoeAttack -= SkillsSelected;
	}

	void EnemyDodgeMethod()
	{
		int dodgeRandom = Random.Range(1, 100);
		enemyDodge = dodgeRandom <= dodgeChance ? true:false;
	}
	
	public void TakeSingleAttack()
	{
		if(PlayerScript.isCoroutineEnd && !die)
		{
			EnemyDodgeMethod();
			SkillsSelected();
			PlayerScript.isCoroutineEnd = false;
		}
	}

	public void SendDamage()
	{
		if(!die)
		{
			int dodgeRandom = Random.Range(1, 100);
			bool dodge = dodgeRandom <= playerDogdeChance ? true:false;
			if(!dodge)
			{
				int enemyCritRandom = Random.Range(1, 100);
				enemyCriticalStrike = enemyCritRandom <= critChance ? true:false;
				if(enemyCriticalStrike)
				{
					PlayerScript.instance.ReceiveDamage(currentDamage * 1.5f, enemyDamageType, true); // отправляем критический урон игроку
					Debug.LogWarning("enemyCriticalStrike");
				}
				else
				{
					PlayerScript.instance.ReceiveDamage(currentDamage, enemyDamageType, false); // отправляем урон игроку
				}
			}
			else
			{
				print("MISS");
				Global.EventMethod(new Vector3(0, 120, 0), PlayerScript.instance.gameObject, "Miss!", 0);
			}
		}
	}

	public void ReceiveDamage(float modificator, bool isWithoutPassiveSkills, ElementType damageType)
	{
		if(hpSlider.value != 0f)
		{
			int critRandom = Random.Range(1, 100);
			bool criticalStrike = critRandom <= (PlayerStats.instance.critChance + skills.extraCritChance) ? true:false;
			float incomingDmg;

			if(isWithoutPassiveSkills)
			{
				enemyDodge = false;
				criticalStrike = false;
			}

			if(!enemyDodge)
			{
				if(weakness == damageType)
				{
					if(criticalStrike)
					{
						incomingDmg = (Random.Range(PlayerStats.instance.physicalDamageMin, PlayerStats.instance.physicalDamageMax) + Global.currentElemntalDmg) * armor * 2f * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "!!" + (int)incomingDmg, 2);
						Debug.LogWarning("criticalStrike");
					}
					else
					{
						incomingDmg = (Random.Range(PlayerStats.instance.physicalDamageMin, PlayerStats.instance.physicalDamageMax) + Global.currentElemntalDmg) * armor * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "-!" + (int)incomingDmg, 1);
					}
				}
				else
				{
					if(criticalStrike)
					{
						incomingDmg = Random.Range(PlayerStats.instance.physicalDamageMin, PlayerStats.instance.physicalDamageMax) * armor * 2f * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "!" + (int)incomingDmg, 2);
						Debug.LogWarning("criticalStrike");
					}
					else
					{
						incomingDmg = Random.Range(PlayerStats.instance.physicalDamageMin, PlayerStats.instance.physicalDamageMax) * armor * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "-" + (int)incomingDmg, 1);
					}
				}
			}
			else
			{
				print("MISS");
				Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "Miss!", 0);
			}

		}
	}

	public void ReceiveDamageAOE(float modificator, bool isWithoutPassiveSkills, ElementType damageType)
	{
		if(hpSlider.value != 0f)
		{
			int critRandom = Random.Range(1, 100);
			bool criticalStrike = critRandom <= (PlayerStats.instance.critChance + skills.extraCritChance) ? true:false;
			float incomingDmg;

			if(isWithoutPassiveSkills)
			{
				enemyDodge = false;
				criticalStrike = false;
			}
	
			if(!enemyDodge)
			{
				if(weakness == damageType)
				{
					if(criticalStrike)
					{
						incomingDmg = (Random.Range (PlayerStats.instance.magicDamageMin, PlayerStats.instance.magicDamageMax) + Global.currentMagicElemntalDmg) * armor * 1.5f * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "!!" + (int)incomingDmg, 2);
						Debug.LogWarning("MagicCriticalStrike");
					}
					else
					{
						incomingDmg = (Random.Range (PlayerStats.instance.magicDamageMin, PlayerStats.instance.magicDamageMax) + Global.currentMagicElemntalDmg) * armor * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "-!" + (int)incomingDmg, 1);
						print("test" + weakness);
					}
				}
				else
				{
					if(criticalStrike)
					{
						incomingDmg = Random.Range (PlayerStats.instance.magicDamageMin, PlayerStats.instance.magicDamageMax) * armor * 1.5f * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "!" + (int)incomingDmg, 2);
						Debug.LogWarning("MagicCriticalStrike");
					}
					else
					{
						incomingDmg = Random.Range (PlayerStats.instance.magicDamageMin, PlayerStats.instance.magicDamageMax) * armor * modificator;
						hpSlider.value -= incomingDmg / maxHp;
						Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "-" + (int)incomingDmg, 1);
					}
				}
			}
			else
			{
				print("MISS");
				Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "Miss!", 0);
			}
		}
	}
	
	public void Drop()
	{
		if(die)
		{
			int randomMultiplier = Random.Range(0, 4);
			int earnedGold = (level * randomMultiplier) / 2; 
			Global.gold += earnedGold;

			int randomItem = Random.Range(1, 100);
			bool rareDrop = randomItem <= 5 ? true:false;
			if(rareDrop)
			{
				int randomDrop = Random.Range(1, 5);
				switch (randomDrop)
				{
				case 1:
					print("<= 10");
					Destroy(gameObject);
					break;
				case 2:
					print("<= 10");
					Destroy(gameObject);
					break;
				case 3:
					print("<= 10");
					Destroy(gameObject);
					break;
				case 4:
					print("<= 10");
					Destroy(gameObject);
					break;
				}
			}
			else
			{
				int randomDrop = Random.Range(1, 6);
				switch (randomDrop)
				{
				case 1:
					print("Drop");
//					GlobalScript.potionAmount += 1;
					Destroy(gameObject);
					break;
				case 2:
					print("Drop");
					Global.armorMaterial += 1;
					Destroy(gameObject);
					break;
				case 3:
					print("Drop");
					Global.mWeaponMaterial += 1;
					Destroy(gameObject);
					break;
				case 4:
					print("Drop");
					Global.sWeaponMaterial += 1;
					Destroy(gameObject);
					break;
				case 5:
					print("Drop");
					Global.fireElement += 1;
					Destroy(gameObject);
					break;
				}
			}
		}
		Global.invUpdate();
	}

	void LabelHP()
	{
		hpLabel.text = Mathf.RoundToInt(UIProgressBar.current.value * maxHp) + " HP";
	}

	void Update ()
	{
		if(hpSlider.value <= 0)
		{
			die = true;
			print ("Enemy is die");
		}
	}

	void SkillsSelected()
	{
		switch (Global.ElementDamageType)
		{
		case ElementType.Earth:
			PlayerScript.instance.SendDamage();
			break;
		case ElementType.Fire:
			if(Skills.isFireDOTselected)
			{
				if(fireDOTtime == 3 && !enemyDodge)
				{
					Global.fireDOT += skills.FireDOT;
					Global.SpellStatusCreate(statusBar, fireDOTtime - 1, "fireDOT", new Color32(238, 117, 117, 255));
					statusBar.GetComponent<UIGrid>().repositionNow = true;
					print ("кол-во подписчиков на fireDOT: " + Global.fireDOT.GetInvocationList().Length);
				}
				else if(fireDOTtime > 0 && !enemyDodge)
				{
					fireDOTtime = 3;
				}
				else 
				{
					print("miss");
					Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "Miss!", 0);
				}
				PlayerScript.instance.SendDamage();
			}
			else if(Skills.isFireDOTselectedAOE)
			{
				if(AOEfireDOTtime == 3 && !enemyDodge)
				{
					Global.AOEfireDOT += skills.AOEfireDOT;
					Global.SpellStatusCreate(statusBar, AOEfireDOTtime - 1, "AOEfireDOT", new Color32(238, 117, 117, 255));
					statusBar.GetComponent<UIGrid>().repositionNow = true;
					print ("кол-во подписчиков на AOEfireDOT: " + Global.AOEfireDOT.GetInvocationList().Length);
				}
				else if(AOEfireDOTtime > 0 && !enemyDodge)
				{
					AOEfireDOTtime = 3;
				}
				else 
				{
					print("miss");
					Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "Miss!", 0);
				}
			}
			break;
		case ElementType.Water:
			PlayerScript.instance.SendDamage();
			break;
		case ElementType.Wind:
			if(Skills.isFireDOTselected)
			{
				if(critTime == 2)
				{
					Global.SpellStatusCreate(statusBar, critTime - 1, "extraCrit", new Color32(167, 167, 167, 255));
					statusBar.GetComponent<UIGrid>().repositionNow = true;
					Global.wind += skills.ExtraCritChanceMethod;
				}
				PlayerScript.instance.SendDamage();
			}
			break;
		}
	}
	
//	void SkillsSelectedAOE()
//	{
//		switch (Global.magicElementDamageType)
//		{
//		case ElementType.Earth:
//			
//			PlayerScript.instance.SendDamage();
//			break;
//		case ElementType.Fire:
//			if(Skills.isFireDOTselectedAOE)
//			{
//				if(AOEfireDOTtime == 3 && !enemyDodge)
//				{
//					Global.AOEfireDOT += skills.AOEfireDOT;
//					Global.SpellStatusCreate(statusBar, AOEfireDOTtime - 1, "AOEfireDOT", new Color32(238, 117, 117, 255));
//					statusBar.GetComponent<UIGrid>().repositionNow = true;
//					print ("кол-во подписчиков на AOEfireDOT: " + Global.AOEfireDOT.GetInvocationList().Length);
//				}
//				else if(AOEfireDOTtime > 0 && !enemyDodge)
//				{
//					AOEfireDOTtime = 3;
//				}
//				else 
//				{
//					print("miss");
//					Global.EventMethod(new Vector3 (0, -30, 0), gameObject, "Miss!", 0);
//				}
//			}
//			break;
//		case ElementType.Water:
//			
//			PlayerScript.instance.SendDamage();
//			break;
//		case ElementType.Wind:
//			
//			PlayerScript.instance.SendDamage();
//			break;
//		}
//	}
}
