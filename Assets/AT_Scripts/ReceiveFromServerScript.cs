﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Networking;
using System;

public class ReceiveFromServerScript : MonoBehaviour
{
	string auth = "http://localhost/project_4/authorization.php";
	string items = "http://localhost/project_4/getItems.php";

	public PlayerStats playerStats;

	public ItemList list;

//	public bool test;
//	public string st;
//	test = Convert.ToBoolean(st);

	IEnumerator Start()
	{
		using(UnityWebRequest www = UnityWebRequest.Get(auth)) 
		{
			yield return www.Send();
			if(www.isError)
			{
				Debug.Log(www.error);
			}
			else
			{
				string json = www.downloadHandler.text;
				Debug.Log(www.downloadHandler.text);
				PlayerStats.instance = JsonUtility.FromJson<PlayerStats>(json);
				playerStats = PlayerStats.instance;
				ItemDataBase.DBLoad();
				yield return StartCoroutine("GetItmes");
			}
		}
	}

	IEnumerator GetItmes()
	{
		using(UnityWebRequest www = UnityWebRequest.Get(items)) 
		{
			yield return www.Send();
			if(www.isError)
			{
				Debug.Log(www.error);
			}
			else
			{
				string json = www.downloadHandler.text;
				Debug.Log(www.downloadHandler.text);
				ItemList.instance = JsonUtility.FromJson<ItemList>(json);
				list = ItemList.instance;
				yield return StartCoroutine("Equip");
			}
		}
	}

	IEnumerator Equip()
	{
		yield return new WaitForSeconds(0f);

		for (int i = 0; i < ItemList.instance.onCharacter.Count; i++)
		{
			switch (ItemList.instance.onCharacter[i].itemType)
			{
			case ItemType.Head:
				CharacterManagerScript.headIndex = i;
				CharacterManagerScript.isHeadEquipped = true;
				break;
			case ItemType.Shoulder:
				CharacterManagerScript.shoulderIndex = i;
				CharacterManagerScript.isShoulderEquipped = true;
				break;
			case ItemType.Gloves:
				CharacterManagerScript.glovesIndex = i;
				CharacterManagerScript.isGlovesEquipped = true;
				break;
			case ItemType.MainHand:
				CharacterManagerScript.mainHandIndex = i;
				CharacterManagerScript.isMainHandEquipped = true;
				break;
			case ItemType.OffHand:
				CharacterManagerScript.offHandIndex = i;
				CharacterManagerScript.isOffHandEquipped = true;
				break;
			case ItemType.Boots:
				CharacterManagerScript.bootsIndex = i;
				CharacterManagerScript.isBootsEquipped = true;
				break;
			case ItemType.Leggings:
				CharacterManagerScript.leggingsIndex = i;
				CharacterManagerScript.isLeggingsEquipped = true;
				break;
			case ItemType.Belt:
				CharacterManagerScript.beltIndex = i;
				CharacterManagerScript.isBeltEquipped = true;
				break;
			case ItemType.Armor:
				CharacterManagerScript.armorIndex = i;
				CharacterManagerScript.isArmorEquipped = true;
				break;
			case ItemType.Relic:
				CharacterManagerScript.relicIndex = i;
				CharacterManagerScript.isRelicEquipped = true;
				break;
			case ItemType.Necklace:
				CharacterManagerScript.necklaceIndex = i;
				CharacterManagerScript.isNecklaceEquipped = true;
				break;
			}
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.A))
		{

		}
	}
}