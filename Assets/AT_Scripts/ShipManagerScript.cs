﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipManagerScript : MonoBehaviour 
{
	public TweenAlpha info;
	public TweenAlpha fade;

	public UILabel titleLabel;
	public UILabel roomInfo;
	public UILabel buttonLabel;
	public UILabel priceLabel;
	public UILabel levelLabel;

	public UISprite icon;

	public UIGrid[] grids;

	public GameObject[] room0Buttons;
	public GameObject[] room1Buttons;
	public GameObject[] room2Buttons;
	public GameObject[] room3Buttons;
	public GameObject[] room4Buttons;
	public GameObject[] room5Buttons;
	public GameObject[] room6Buttons;
	public GameObject[] room7Buttons;
	public GameObject[] room8Buttons;

	public GameObject[] rooms0;
	public GameObject[] rooms1;
	public GameObject[] rooms2;
	public GameObject[] rooms3;
	public GameObject[] rooms4;
	public GameObject[] rooms5;
	public GameObject[] rooms6;
	public GameObject[] rooms7;
	public GameObject[] rooms8;

	GameObject[][] cabin = new GameObject[9][]; 
	GameObject[][] roomButtons = new GameObject[9][]; 

	int number;

	void Start()
	{
		cabin[0] = rooms0;
		cabin[1] = rooms1;
		cabin[2] = rooms2;
		cabin[3] = rooms3;
		cabin[4] = rooms4;
		cabin[5] = rooms5;
		cabin[6] = rooms6;
		cabin[7] = rooms7;
		cabin[8] = rooms8;

		roomButtons[0] = room0Buttons;
		roomButtons[1] = room1Buttons;
		roomButtons[2] = room2Buttons;
		roomButtons[3] = room3Buttons;
		roomButtons[4] = room4Buttons;
		roomButtons[5] = room5Buttons;
		roomButtons[6] = room6Buttons;
		roomButtons[7] = room7Buttons;
		roomButtons[8] = room8Buttons;

//		Ship.room0 = RoomType.Kitchen;
//		WorkerList.instance.AddKitchenWorker("test", 1);

		StartCheck();
	}

	void StartCheck()
	{
		if(Ship.room0 != RoomType.Empty)
		{
			cabin[0][0].SetActive(false);
			cabin[0][(int)Ship.room0].SetActive(true);
			for (int i = 0; i < room0Buttons.Length; i++) 
			{
				room0Buttons[i].SetActive(false);
			}
		}
		if(Ship.room1 != RoomType.Empty)
		{
			cabin[1][0].SetActive(false);
			cabin[1][(int)Ship.room1].SetActive(true);
			for (int i = 0; i < room1Buttons.Length; i++) 
			{
				room1Buttons[i].SetActive(false);
			}
		}
		if(Ship.room2 != RoomType.Empty)
		{
			cabin[2][0].SetActive(false);
			cabin[2][(int)Ship.room2].SetActive(true);
			for (int i = 0; i < room2Buttons.Length; i++) 
			{
				room2Buttons[i].SetActive(false);
			}
		}	
		if(Ship.room3 != RoomType.Empty)
		{
			cabin[3][0].SetActive(false);
			cabin[3][(int)Ship.room3].SetActive(true);
			for (int i = 0; i < room3Buttons.Length; i++) 
			{
				room3Buttons[i].SetActive(false);
			}
		}
		if(Ship.room4 != RoomType.Empty)
		{
			cabin[4][0].SetActive(false);
			cabin[4][(int)Ship.room4].SetActive(true);
			for (int i = 0; i < room4Buttons.Length; i++) 
			{
				room4Buttons[i].SetActive(false);
			}
		}
		if(Ship.room5 != RoomType.Empty)
		{
			cabin[5][0].SetActive(false);
			cabin[5][(int)Ship.room5].SetActive(true);
			for (int i = 0; i < room5Buttons.Length; i++) 
			{
				room3Buttons[i].SetActive(false);
			}
		}
		if(Ship.room6 != RoomType.Empty)
		{
			cabin[6][0].SetActive(false);
			cabin[6][(int)Ship.room6].SetActive(true);
			for (int i = 0; i < room6Buttons.Length; i++) 
			{
				room6Buttons[i].SetActive(false);
			}
		}
		if(Ship.room7 != RoomType.Empty)
		{
			cabin[7][0].SetActive(false);
			cabin[7][(int)Ship.room7].SetActive(true);
			for (int i = 0; i < room7Buttons.Length; i++) 
			{
				room7Buttons[i].SetActive(false);
			}
		}
		if(Ship.room8 != RoomType.Empty)
		{
			cabin[8][0].SetActive(false);
			cabin[8][(int)Ship.room8].SetActive(true);
			for (int i = 0; i < room8Buttons.Length; i++) 
			{
				room8Buttons[i].SetActive(false);
			}
		}

		for (int i = 0; i < roomButtons.Length; i++)
		{
			for (int j = 1; j < 10; j++)
			{
				if(Ship.room0 != RoomType.Empty && (int)Ship.room0 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room1 != RoomType.Empty && (int)Ship.room1 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room2 != RoomType.Empty && (int)Ship.room2 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room3 != RoomType.Empty && (int)Ship.room3 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room4 != RoomType.Empty && (int)Ship.room4 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room5 != RoomType.Empty && (int)Ship.room5 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room6 != RoomType.Empty && (int)Ship.room6 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room7 != RoomType.Empty && (int)Ship.room7 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
				if(Ship.room8 != RoomType.Empty && (int)Ship.room8 == j)
				{
					roomButtons[i][j - 1].SetActive(false);
				}
			}
		}

		for (int i = 0; i < cabin.Length; i++) 
		{
			for (int j = 1; j < cabin[i].Length; j++) 
			{
				if(cabin[i][j].activeInHierarchy)
				{
					switch (j)
					{
					case 1:
						for (int a = 0; a < WorkerList.instance.kitchenWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/KitchenWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 2:
						for (int a = 0; a < WorkerList.instance.gardenWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/GardenWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 3:
						for (int a = 0; a < WorkerList.instance.boilerWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/BoilerWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 4:
						for (int a = 0; a < WorkerList.instance.mineWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/MineWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 5:
						for (int a = 0; a < WorkerList.instance.forgeWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/ForgeWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 6:
						for (int a = 0; a < WorkerList.instance.barackWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/BarackWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 7:
						for (int a = 0; a < WorkerList.instance.libraryWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/LibraryWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 8:
						for (int a = 0; a < WorkerList.instance.tradeWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/TradeWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					case 9:
						for (int a = 0; a < WorkerList.instance.enchantmentWorkers.Count; a++)
						{
							GameObject prefab = Resources.Load("Prefabs/EnchantmentWorker") as GameObject;
							GameObject postfab = cabin[i][j].AddChild(prefab);
							postfab.transform.localPosition = new Vector2(0, 10);
						}
						break;
					}
				}
			}
		}

		for (int i = 0; i < grids.Length; i++)
		{
			grids[i].repositionNow = true;
		}

		print(Ship.room0);
		print(Ship.room1);
		print(Ship.room2);
		print(Ship.room3);
	}

	public void InfoPanel()
	{
		info.PlayForward();
		fade.PlayForward();
	}

	public void Button() //upgrade or buy
	{
		switch (titleLabel.text)
		{
		case "Kitchen":	
			if(Ship.kitchenLevel < 1)
			{
				BuyKitchen();
				StartCheck();
			}
			else
			{
				Ship.kitchenLevel ++;
			}
			Kitchen();
			break;
		case "Garden":
			if(Ship.gardenLevel < 1)
			{
				BuyGarden();
				StartCheck();
			}
			else
			{
				Ship.gardenLevel ++;
			}
			Garden();
			break;
		case "Boiler":
			if(Ship.boilerLevel < 1)
			{
				BuyBoiler();
				StartCheck();
			}
			else
			{
				Ship.boilerLevel ++;
			}
			Boiler();
			break;
		case "Mine":
			if(Ship.mineLevel < 1)
			{
				BuyMine();
				StartCheck();
			}
			else 
			{
				Ship.mineLevel ++;
			}
			Mine();
			break;
		case "Forge":
			if(Ship.forgeLevel < 1)
			{
				BuyForge();
				StartCheck();
			}
			else 
			{
				Ship.forgeLevel ++;
			}
			Forge();
			break;
		case "Barack":
			if(Ship.barackLevel < 1)
			{
				BuyBarack();
				StartCheck();
			}
			else 
			{
				Ship.barackLevel ++;
			}
			Barack();
			break;
		case "Library":
			if(Ship.libraryLevel < 1)
			{
				BuyLibrary();
				StartCheck();
			}
			else 
			{
				Ship.barackLevel ++;
			}
			Library();
			break;
		case "Trader":
			if(Ship.traderLevel < 1)
			{
				BuyTrader();
				StartCheck();
			}
			else 
			{
				Ship.traderLevel ++;
			}
			Trader();
			break;
		case "Enchantment":
			if(Ship.enchantmentLevel < 1)
			{
				BuyEnchantment();
				StartCheck();
			}
			else 
			{
				Ship.enchantmentLevel ++;
			}
			Enchantment();
			break;
		case "KitchenWorker":
			if(WorkerList.instance.kitchenWorkers.Count == number)
			{
				WorkerList.instance.AddKitchenWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.kitchenWorkers[number].workerLevel++;
			}
			KitchenWorker();
			break;
		case "GardenWorker":
			if(WorkerList.instance.gardenWorkers.Count == number)
			{
				WorkerList.instance.AddGardenWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.gardenWorkers[number].workerLevel++;
			}
			GardenWorker();
			break;
		case "BoilerWorker":
			if(WorkerList.instance.boilerWorkers.Count == number)
			{
				WorkerList.instance.AddBoilerWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.boilerWorkers[number].workerLevel++;
			}
			BoilerWorker();
			break;
		case "MineWorker":
			if(WorkerList.instance.mineWorkers.Count == number)
			{
				WorkerList.instance.AddMineWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.mineWorkers[number].workerLevel++;
			}
			MineWorker();
			break;
		case "ForgeWorker":
			if(WorkerList.instance.forgeWorkers.Count == number)
			{
				WorkerList.instance.AddForgeWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.forgeWorkers[number].workerLevel++;
			}
			ForgeWorker();
			break;
		case "BarackWorker":
			if(WorkerList.instance.barackWorkers.Count == number)
			{
				WorkerList.instance.AddBarackWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.barackWorkers[number].workerLevel++;
			}
			BarackWorker();
			break;
		case "LibraryWorker":
			if(WorkerList.instance.libraryWorkers.Count == number)
			{
				WorkerList.instance.AddLibraryWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.libraryWorkers[number].workerLevel++;
			}
			LibraryWorker();
			break;
		case "TradeWorker":
			if(WorkerList.instance.tradeWorkers.Count == number)
			{
				WorkerList.instance.AddTradeWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.tradeWorkers[number].workerLevel++;
			}
			TradeWorker();
			break;
		case "EnchantmentWorker":
			if(WorkerList.instance.enchantmentWorkers.Count == number)
			{
				WorkerList.instance.AddEnchantmentWorker("test", 1);
				StartCheck();
			}
			else 
			{
				WorkerList.instance.enchantmentWorkers[number].workerLevel++;
			}
			EnchantmentWorker();
			break;
		}
	}

	void BuyKitchen()
	{
		Ship.kitchenLevel ++;
		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Kitchen;
			break;
		case 1:
			Ship.room1 = RoomType.Kitchen;
			break;
		case 2:
			Ship.room2 = RoomType.Kitchen;
			break;
		case 3:
			Ship.room3 = RoomType.Kitchen;
			break;
		case 4:
			Ship.room4 = RoomType.Kitchen;
			break;
		case 5:
			Ship.room5 = RoomType.Kitchen;
			break;
		case 6:
			Ship.room6 = RoomType.Kitchen;
			break;
		case 7:
			Ship.room7 = RoomType.Kitchen;
			break;
		case 8:
			Ship.room8 = RoomType.Kitchen;
			break;
		}
	}

	void BuyGarden()
	{
		Ship.gardenLevel ++;
		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Garden;
			break;
		case 1:
			Ship.room1 = RoomType.Garden;
			break;
		case 2:
			Ship.room2 = RoomType.Garden;
			break;
		case 3:
			Ship.room3 = RoomType.Garden;
			break;
		case 4:
			Ship.room4 = RoomType.Garden;
			break;
		case 5:
			Ship.room5 = RoomType.Garden;
			break;
		case 6:
			Ship.room6 = RoomType.Garden;
			break;
		case 7:
			Ship.room7 = RoomType.Garden;
			break;
		case 8:
			Ship.room8 = RoomType.Garden;
			break;
		}
	}

	void BuyBoiler()
	{
		Ship.boilerLevel ++;
		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Boiler;
			break;
		case 1:
			Ship.room1 = RoomType.Boiler;
			break;
		case 2:
			Ship.room2 = RoomType.Boiler;
			break;
		case 3:
			Ship.room3 = RoomType.Boiler;
			break;
		case 4:
			Ship.room4 = RoomType.Boiler;
			break;
		case 5:
			Ship.room5 = RoomType.Boiler;
			break;
		case 6:
			Ship.room6 = RoomType.Boiler;
			break;
		case 7:
			Ship.room7 = RoomType.Boiler;
			break;
		case 8:
			Ship.room8 = RoomType.Boiler;
			break;
		}
	}

	void BuyMine()
	{
		Ship.mineLevel ++;

		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Mine;
			break;
		case 1:
			Ship.room1 = RoomType.Mine;
			break;
		case 2:
			Ship.room2 = RoomType.Mine;
			break;
		case 3:
			Ship.room3 = RoomType.Mine;
			break;
		case 4:
			Ship.room4 = RoomType.Mine;
			break;
		case 5:
			Ship.room5 = RoomType.Mine;
			break;
		case 6:
			Ship.room6 = RoomType.Mine;
			break;
		case 7:
			Ship.room7 = RoomType.Mine;
			break;
		case 8:
			Ship.room8 = RoomType.Mine;
			break;
		}
	}

	void BuyForge()
	{
		Ship.forgeLevel ++;

		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Forge;
			break;
		case 1:
			Ship.room1 = RoomType.Forge;
			break;
		case 2:
			Ship.room2 = RoomType.Forge;
			break;
		case 3:
			Ship.room3 = RoomType.Forge;
			break;
		case 4:
			Ship.room4 = RoomType.Forge;
			break;
		case 5:
			Ship.room5 = RoomType.Forge;
			break;
		case 6:
			Ship.room6 = RoomType.Forge;
			break;
		case 7:
			Ship.room7 = RoomType.Forge;
			break;
		case 8:
			Ship.room8 = RoomType.Forge;
			break;
		}
	}

	void BuyBarack()
	{
		Ship.barackLevel ++;

		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Barack;
			break;
		case 1:
			Ship.room1 = RoomType.Barack;
			break;
		case 2:
			Ship.room2 = RoomType.Barack;
			break;
		case 3:
			Ship.room3 = RoomType.Barack;
			break;
		case 4:
			Ship.room4 = RoomType.Barack;
			break;
		case 5:
			Ship.room5 = RoomType.Barack;
			break;
		case 6:
			Ship.room6 = RoomType.Barack;
			break;
		case 7:
			Ship.room7 = RoomType.Barack;
			break;
		case 8:
			Ship.room8 = RoomType.Barack;
			break;
		}
	}

	void BuyLibrary()
	{
		Ship.libraryLevel ++;

		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Library;
			break;
		case 1:
			Ship.room1 = RoomType.Library;
			break;
		case 2:
			Ship.room2 = RoomType.Library;
			break;
		case 3:
			Ship.room3 = RoomType.Library;
			break;
		case 4:
			Ship.room4 = RoomType.Library;
			break;
		case 5:
			Ship.room5 = RoomType.Library;
			break;
		case 6:
			Ship.room6 = RoomType.Library;
			break;
		case 7:
			Ship.room7 = RoomType.Library;
			break;
		case 8:
			Ship.room8 = RoomType.Library;
			break;
		}
	}

	void BuyTrader()
	{
		Ship.traderLevel ++;

		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Trader;
			break;
		case 1:
			Ship.room1 = RoomType.Trader;
			break;
		case 2:
			Ship.room2 = RoomType.Trader;
			break;
		case 3:
			Ship.room3 = RoomType.Trader;
			break;
		case 4:
			Ship.room4 = RoomType.Trader;
			break;
		case 5:
			Ship.room5 = RoomType.Trader;
			break;
		case 6:
			Ship.room6 = RoomType.Trader;
			break;
		case 7:
			Ship.room7 = RoomType.Trader;
			break;
		case 8:
			Ship.room8 = RoomType.Trader;
			break;
		}
	}

	void BuyEnchantment()
	{
		Ship.enchantmentLevel ++;

		switch (number)
		{
		case 0:
			Ship.room0 = RoomType.Enchantment;
			break;
		case 1:
			Ship.room1 = RoomType.Enchantment;
			break;
		case 2:
			Ship.room2 = RoomType.Enchantment;
			break;
		case 3:
			Ship.room3 = RoomType.Enchantment;
			break;
		case 4:
			Ship.room4 = RoomType.Enchantment;
			break;
		case 5:
			Ship.room5 = RoomType.Enchantment;
			break;
		case 6:
			Ship.room6 = RoomType.Enchantment;
			break;
		case 7:
			Ship.room7 = RoomType.Enchantment;
			break;
		case 8:
			Ship.room8 = RoomType.Enchantment;
			break;
		}
	}

	public void Kitchen()
	{
		titleLabel.text = "Kitchen";
		InfoPanel();
		if(Ship.kitchenLevel < 1)
		{
			roomInfo.text = "Kitchen brings you food";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceKitchen;
			levelLabel.text = "Level: " + Ship.kitchenLevel;
		}
		else
		{
			roomInfo.text = string.Format("Kitchen brings you food {0} \n Next level food - {1}", Ship.food, (Ship.food * Ship.kitchenLevel));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceKitchen * Ship.kitchenLevel);
			levelLabel.text = "Level: " + Ship.kitchenLevel;
		}
			
	}

	public void KitchenWorker()
	{
		titleLabel.text = "KitchenWorker";
		InfoPanel();

		if(WorkerList.instance.kitchenWorkers.Count == number)
		{
			roomInfo.text = "Kitchen Worker brings you food";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceKitchenWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Kitchen Worker brings you food {0} \n Next level food - {1}", Ship.food, (Ship.food * Ship.kitchenLevel));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceKitchenWorker * WorkerList.instance.kitchenWorkers[number].workerLevel);
			levelLabel.text = "Level: " + + WorkerList.instance.kitchenWorkers[number].workerLevel;
		}
	}

	public void Garden()
	{
		titleLabel.text = "Garden";
		InfoPanel();

		if(Ship.gardenLevel < 1)
		{
			roomInfo.text = "Garden brings you products for Kitchen";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceGarden;
			levelLabel.text = "Level: " + Ship.gardenLevel;
		}
		else
		{
			roomInfo.text = string.Format("Garden brings you products {0} \n Next level products - {1}", Ship.products, (Ship.products * Ship.gardenLevel));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceGarden * Ship.gardenLevel);
			levelLabel.text = "Level: " + Ship.gardenLevel;
		}
	}

	public void GardenWorker()
	{
		titleLabel.text = "GardenWorker";
		InfoPanel();

		if(WorkerList.instance.gardenWorkers.Count == number)
		{
			roomInfo.text = "Garden Worker brings you products for Kitchen";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceGardenWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Garden brings you products {0} \n Next level products - {1}", Ship.products, (Ship.products * Ship.gardenLevel));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceGardenWorker * WorkerList.instance.gardenWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.gardenWorkers[number].workerLevel;
		}
	}

	public void Boiler()
	{
		titleLabel.text = "Boiler";
		InfoPanel();

		if(Ship.gardenLevel < 1)
		{
			roomInfo.text = "Boiler brings you energy for engine ship";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceBoiler;
			levelLabel.text = "Level: " + Ship.boilerLevel;
		}
		else
		{
			roomInfo.text = string.Format("Boiler brings you energy {0} \n Next level energy - {1}", Ship.energy, (Ship.energy * Ship.boilerLevel));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceBoiler * Ship.boilerLevel);
			levelLabel.text = "Level: " + Ship.boilerLevel;
		}
	}

	public void BoilerWorker()
	{
		titleLabel.text = "BoilerWorker";
		InfoPanel();

		if(WorkerList.instance.boilerWorkers.Count == number)
		{
			roomInfo.text = "Boiler worker brings you energy for engine ship";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceBoilerWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Boiler brings you energy {0} \n Next level energy - {1}", Ship.energy, (Ship.energy * Ship.boilerLevel));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceGardenWorker * WorkerList.instance.boilerWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.boilerWorkers[number].workerLevel;
		}
	}

	public void Mine()
	{
		titleLabel.text = "Mine";
		InfoPanel();

		if(Ship.gardenLevel < 1)
		{
			roomInfo.text = "Mine brings you minerals";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceMine;
			levelLabel.text = "Level: " + Ship.mineLevel;
		}
		else
		{
			roomInfo.text = string.Format("Mine brings you minerals {0} \n Next level minerals - {1}", Ship.minerals, (Ship.minerals * Ship.priceMine));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceMine * Ship.mineLevel);
			levelLabel.text = "Level: " + Ship.mineLevel;
		}
	}

	public void MineWorker()
	{
		titleLabel.text = "MineWorker";
		InfoPanel();

		if(WorkerList.instance.mineWorkers.Count == number)
		{
			roomInfo.text = "Mine brings you minerals";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceMineWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Mine brings you minerals {0} \n Next level minerals - {1}", Ship.minerals, (Ship.minerals * Ship.priceMine));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceGardenWorker * WorkerList.instance.mineWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.mineWorkers[number].workerLevel;
		}
	}

	public void Forge()
	{
		titleLabel.text = "Forge";
		InfoPanel();

		if(Ship.forgeLevel < 1)
		{
			roomInfo.text = "Forge brings you equipments";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceForge;
			levelLabel.text = "Level: " + Ship.forgeLevel;
		}
		else
		{
			roomInfo.text = string.Format("Forge brings you equipments {0} \n Next level equipments - {1}", Ship.equipemnts, (Ship.equipemnts * Ship.priceForge));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceForge * Ship.forgeLevel);
			levelLabel.text = "Level: " + Ship.forgeLevel;
		}
	}

	public void ForgeWorker()
	{
		titleLabel.text = "ForgeWorker";
		InfoPanel();

		if(WorkerList.instance.forgeWorkers.Count == number)
		{
			roomInfo.text = "Forge workers brings you equipments";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceForgeWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Forge workers brings you equipments {0} \n Next level equipments - {1}", Ship.equipemnts, (Ship.equipemnts * Ship.priceForge));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceForgeWorker * WorkerList.instance.forgeWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.forgeWorkers[number].workerLevel;
		}
	}
		
	public void Barack()
	{
		titleLabel.text = "Barack";
		InfoPanel();

		if(Ship.barackLevel < 1)
		{
			roomInfo.text = "Barack brings you worriors";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceBarack;
			levelLabel.text = "Level: " + Ship.barackLevel;
		}
		else
		{
			roomInfo.text = string.Format("Barack brings you warriors {0} \n Next level warriors - {1}", Ship.warriors, (Ship.warriors * Ship.priceBarack));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceBarack * Ship.barackLevel);
			levelLabel.text = "Level: " + Ship.barackLevel;
		}
	}

	public void BarackWorker()
	{
		titleLabel.text = "BarackWorker";
		InfoPanel();

		if(WorkerList.instance.barackWorkers.Count == number)
		{
			roomInfo.text = "Barack workers brings you worriors";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceBarackWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Barack workers brings you warriors {0} \n Next level warriors - {1}", Ship.warriors, (Ship.warriors * Ship.priceBarack));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceBarackWorker * WorkerList.instance.barackWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.barackWorkers[number].workerLevel;
		}
	}

	public void Library()
	{
		titleLabel.text = "Library";
		InfoPanel();

		if(Ship.libraryLevel < 1)
		{
			roomInfo.text = "Library brings you letters";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceLibrary;
			levelLabel.text = "Level: " + Ship.libraryLevel;
		}
		else
		{
			roomInfo.text = string.Format("Library brings you letters {0} \n Next level letters - {1}", Ship.letters, (Ship.letters * Ship.priceLibrary));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceLibrary * Ship.libraryLevel);
			levelLabel.text = "Level: " + Ship.libraryLevel;
		}
	}

	public void LibraryWorker()
	{
		titleLabel.text = "LibraryWorker";
		InfoPanel();

		if(WorkerList.instance.libraryWorkers.Count == number)
		{
			roomInfo.text = "Library workers brings you letters";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceLibraryWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Library workers brings you letters {0} \n Next level letters - {1}", Ship.letters, (Ship.letters * Ship.priceLibrary));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceLibraryWorker * WorkerList.instance.libraryWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.barackWorkers[number].workerLevel;
		}
	}

	public void Trader()
	{
		titleLabel.text = "Trader";
		InfoPanel();

		if(Ship.libraryLevel < 1)
		{
			roomInfo.text = "Trader brings you money";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceTrader;
			levelLabel.text = "Level: " + Ship.traderLevel;
		}
		else
		{
			roomInfo.text = string.Format("Trader brings you money {0} \n Next level money - {1}", Ship.money, (Ship.money * Ship.priceTrader));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceTrader * Ship.traderLevel);
			levelLabel.text = "Level: " + Ship.traderLevel;
		}
	}

	public void TradeWorker()
	{
		titleLabel.text = "TradeWorker";
		InfoPanel();

		if(WorkerList.instance.tradeWorkers.Count == number)
		{
			roomInfo.text = "Trade workers brings you money";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + (Ship.priceTraderWorker * WorkerList.instance.tradeWorkers[number].workerLevel);
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Trade workers brings you money {0} \n Next level money - {1}", Ship.money, (Ship.money * Ship.priceTrader));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceTraderWorker * WorkerList.instance.tradeWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.tradeWorkers[number].workerLevel;
		}
	}

	public void Enchantment()
	{
		titleLabel.text = "Enchantment";
		InfoPanel();

		if(Ship.libraryLevel < 1)
		{
			roomInfo.text = "Enchantment brings you enchanted items";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceTrader;
			levelLabel.text = "Level: " + Ship.traderLevel;
		}
		else
		{
			roomInfo.text = string.Format("Enchantment brings you enchanted items {0} \n Next level enchanted items - {1}", Ship.enchantment, (Ship.enchantment * Ship.priceEnchantment));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.enchantment * Ship.enchantmentLevel);
			levelLabel.text = "Level: " + Ship.enchantmentLevel;
		}
	}

	public void EnchantmentWorker()
	{
		titleLabel.text = "EnchantmentWorker";
		InfoPanel();

		if(WorkerList.instance.enchantmentWorkers.Count == number)
		{
			roomInfo.text = "Enchantment worker brings you enchanted items";
			buttonLabel.text = "Buy";
			priceLabel.text = "Price: " + Ship.priceEnchantemntWorker;
			levelLabel.text = "Level: 0";
		}
		else
		{
			roomInfo.text = string.Format("Enchantment worker brings you enchanted items {0} \n Next level enchanted items - {1}", Ship.enchantment, (Ship.enchantment * Ship.priceEnchantment));
			buttonLabel.text = "Upgrade";
			priceLabel.text = "Price: " + (Ship.priceEnchantemntWorker * WorkerList.instance.enchantmentWorkers[number].workerLevel);
			levelLabel.text = "Level: " + WorkerList.instance.enchantmentWorkers[number].workerLevel;
		}
	}

	public void Zero()
	{
		number = 0;
	}

	public void One()
	{
		number = 1;
	}

	public void Two()
	{
		number = 2;
	}

	public void Three()
	{
		number = 3;
	}

	public void Four()
	{
		number = 4;
	}

	public void Five()
	{
		number = 5;
	}

	public void Six()
	{
		number = 6;
	}

	public void Seven()
	{
		number = 7;
	}
	public void Eight()
	{
		number = 8;
	}

	void Update() 
	{
		
	}
}
