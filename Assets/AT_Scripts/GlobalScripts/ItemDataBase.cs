﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDataBase 
{
	public static List<Item> equipmentDB = new List<Item>();
	public static List<Item> consumableDB = new List<Item>();

	public static void DBLoad () 
	{
		equipmentDB.Add(new Item(0, 0, 1, "Head", "Armor", 100, ItemType.Head));
		equipmentDB.Add(new Item(1, 0, 1, "Shoulder", "Armor", 100, ItemType.Shoulder));
		equipmentDB.Add(new Item(2, 0, 1, "Gloves", "Armor", 100, ItemType.Gloves));
		equipmentDB.Add(new Item(3, 0, 1, "Sword", "Damage", 100, ItemType.MainHand));
		equipmentDB.Add(new Item(4, 0, 1, "Axe", "Armor", 100, ItemType.OffHand));
		equipmentDB.Add(new Item(5, 0, 1, "Boots", "Damage", 100, ItemType.Boots));
		equipmentDB.Add(new Item(6, 0, 1, "Leggings", "Damage", 100, ItemType.Leggings));
		equipmentDB.Add(new Item(7, 0, 1, "Belt", "Damage", 100, ItemType.Belt));
		equipmentDB.Add(new Item(8, 0, 1, "Armor", "Damage", 100, ItemType.Armor));
		equipmentDB.Add(new Item(9, 0, 1, "Relic", "Damage", 100, ItemType.Relic));
		equipmentDB.Add(new Item(10, 0, 1, "Necklace", "Damage", 100, ItemType.Necklace));

		consumableDB.Add(new Item(0, 0, 10, 1, "Potion", "Heal", 10000, ItemType.Bottle, ItemQuality.Uncommon));
		consumableDB.Add(new Item(1, 0, 10, 1, "Potion", "Heal", 10000, ItemType.Bottle, ItemQuality.Common));
	}
}
