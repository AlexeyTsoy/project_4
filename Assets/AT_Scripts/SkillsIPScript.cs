﻿using UnityEngine;
using System.Collections;

public class SkillsIPScript : MonoBehaviour 
{
	public TweenAlpha info;
	public TweenAlpha fade;

	public UILabel title;
	public UILabel runeTitle1;
	public UILabel runeTitle2;
	public UILabel runeTitle3;
	public UILabel skillInfo1;
	public UILabel skillInfo2;
	public UILabel skillInfo3;
	public UILabel skillPrice1;
	public UILabel skillPrice2;
	public UILabel skillPrice3;
	public UILabel skillLevel1;
	public UILabel skillLevel2;
	public UILabel skillLevel3;

	void InfoPanel()
	{
		info.PlayForward();
		fade.PlayForward();
	}

	public void Burn()
	{
		InfoPanel();
		title.text = gameObject.name;
		runeTitle1.text = "Damage";
		skillInfo1.text = "Damage over time";
		skillPrice1.text = "Price: ";
		skillLevel1.text = "Level: " + Skills.fireDOTrune1Level;

		runeTitle2.text = "Double Attack";
		skillInfo2.text = "Double Attack";
		skillPrice2.text = "Price: ";
		skillLevel2.text = "Level: " + Skills.fireDOTrune2Level;

		runeTitle3.text = "Explosion";
		skillInfo3.text = "Explosion";
		skillPrice3.text = "Price: ";
		skillLevel3.text = "Level: " + Skills.fireDOTrune3Level;
	}

	public void Whip()
	{
		InfoPanel();
		title.text = gameObject.name;
		runeTitle1.text = "Critical Strike";
		skillInfo1.text = "Extra critical chance for 1 round 3% per each upgrade";
		skillPrice1.text = "Price: ";
		skillLevel1.text = "Level: " + Skills.extraCritChanceRune1Level;

		runeTitle2.text = "Rune2";
		skillInfo2.text = "Rune2";
		skillPrice2.text = "Price: ";
		skillLevel2.text = "Level: " + Skills.extraCritChanceRune2Level;

		runeTitle3.text = "Rune3";
		skillInfo3.text = "Rune3";
		skillPrice3.text = "Price: ";
		skillLevel3.text = "Level: " + Skills.extraCritChanceRune3Level;
	}
}
