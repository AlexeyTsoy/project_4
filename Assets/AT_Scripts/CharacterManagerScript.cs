﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterManagerScript : MonoBehaviour 
{
	public static CharacterManagerScript instance;

	public UISprite[] icon = new UISprite[12];
	public UISprite[] frame = new UISprite[12];
	public UIButton[] slot = new UIButton[12];

	public UIGrid tempInventory;

	public UILabel statsLabel;

	public ItemList list; // по сути не нужная переменная

	public UIScrollView scrollView;

	public static bool isHeadEquipped;
	public static bool isShoulderEquipped; // одето ли снаряжение в слот
	public static bool isGlovesEquipped;
	public static bool isMainHandEquipped;
	public static bool isOffHandEquipped;
	public static bool isBootsEquipped;
	public static bool isLeggingsEquipped;
	public static bool isBeltEquipped;
	public static bool isArmorEquipped;
	public static bool isRelicEquipped;
	public static bool isNecklaceEquipped;

	public static int headIndex;
	public static int shoulderIndex; // для хранения индекса в листе onCharacter
	public static int glovesIndex;
	public static int mainHandIndex;
	public static int offHandIndex;
	public static int bootsIndex;
	public static int leggingsIndex;
	public static int beltIndex;
	public static int armorIndex;
	public static int relicIndex;
	public static int necklaceIndex;

	public string title; //переменная для unequip

	int index;

	void Awake()
	{
		instance = this;
	}

	void Start () 
	{
		list = ItemList.instance;

		ItemDataBase.DBLoad();
		HeadEquip();
		ShoulderEquip();
		GlovesEquip();
		MainHandEquip();
		OffhandEquip();
		BootsEquip();
		LeggingsEquip();
		BeltEquip();
		ArmorEquip();
		RelicEquip();
		NecklaceEquip();
		Stats();
		tempInventory.onCustomSort = EqipmentSort;
	}

	public void Head()
	{
		title = "Head";
		infoPanel();

		if(!isHeadEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Head);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = headIndex;
			ItemInfo();
		}
	}

	public void Shoulder()
	{
		title = "Shoulder";
		infoPanel();

		if(!isShoulderEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Shoulder);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = shoulderIndex;
			ItemInfo();
		}
	}

	public void Gloves()
	{
		title = "Gloves";
		infoPanel();

		if(!isGlovesEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Gloves);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = glovesIndex;
			ItemInfo();
		}
	}

	public void Boots()
	{
		title = "Boots";
		infoPanel();

		if(!isBootsEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Boots);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = bootsIndex;
			ItemInfo();
		}
	}

	public void Leggings()
	{
		title = "Leggings";
		infoPanel();

		if(!isLeggingsEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Leggings);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = leggingsIndex;
			ItemInfo();
		}
	}

	public void Belt()
	{
		title = "Belt";
		infoPanel();

		if(!isBeltEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Belt);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = beltIndex;
			ItemInfo();
		}
	}

	public void Armor()
	{
		title = "Armor";
		infoPanel();

		if(!isArmorEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Armor);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = armorIndex;
			ItemInfo();
		}
	}

	public void Relic()
	{
		title = "Relic";
		infoPanel();

		if(!isRelicEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Relic);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = relicIndex;
			ItemInfo();
		}
	}

	public void Necklace()
	{
		title = "Necklace";
		infoPanel();

		if(!isNecklaceEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.Necklace);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = necklaceIndex;
			ItemInfo();
		}
	}

	public void MainHand()
	{
		title = "Main";
		infoPanel();

		if(!isMainHandEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.MainHand);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = mainHandIndex;
			ItemInfo();
		}
	}

	public void Offhand()
	{
		title = "Offhand";
		infoPanel();

		if(!isOffHandEquipped)
		{
			InventoryIPScript.instance.itemName.text = title;
			InventoryIPScript.instance.sortedContent.PlayForward();
			Sorting(ItemType.OffHand);
		}
		else
		{
			InventoryIPScript.instance.itemContent.PlayForward();
			index = offHandIndex;
			ItemInfo();
		}
	}

	public void Sorting(ItemType itemType)
	{
//		tempInventory.onCustomSort = EqipmentSort;

		for (int i = 0; i < ItemList.instance.equipmentItems.Count; i++) 
		{
			if(ItemList.instance.equipmentItems[i].itemType == itemType)
			{
				GameObject obj = PoolingObjectsScript.instance.GetPooledObject();
				SortedItemScript cacheObj = obj.GetComponent<SortedItemScript>();
				cacheObj.index = i;
				cacheObj.Info();
				obj.SetActive(true);
			}
		}
		scrollView.ResetPosition();
		tempInventory.repositionNow = true;
		list = ItemList.instance;
	}

	int EqipmentSort(Transform x , Transform y) 
	{
		var a = x.GetComponent<SortedItemScript>();
		var b = y.GetComponent<SortedItemScript>();
		if (b.quality == a.quality)
		{
			return b.StatsSummary().CompareTo(a.StatsSummary());
		}
		return b.quality.CompareTo(a.quality);
	}

	public void Destroy()
	{
//		tempInventory.onCustomSort -= EqipmentSort;

		foreach (var obj in tempInventory.GetChildList()) 
		{
			NGUITools.SetActive(obj.gameObject, false);
		}
	}

	public void ItemInfo()
	{
		InventoryIPScript.instance.icon.spriteName = ItemList.instance.onCharacter[index].itemIcon;
		InventoryIPScript.instance.frame.spriteName = ItemList.instance.onCharacter[index].itemQuality.ToString();
		InventoryIPScript.instance.itemName.text = "" + ItemList.instance.onCharacter[index].itemName;
		InventoryIPScript.instance.quality.text = "" + ItemList.instance.onCharacter[index].itemQuality;
		InventoryIPScript.instance.mainFeature.text = "" + ItemList.instance.onCharacter[index].itemMainFeature;
		InventoryIPScript.instance.titleFeature.text = "" + ItemList.instance.onCharacter[index].itemTitleFeature;
		InventoryIPScript.instance.agility.text = "Agi: " + ItemList.instance.onCharacter[index].itemAgility;
		InventoryIPScript.instance.intellect.text = "Int: " + ItemList.instance.onCharacter[index].itemIntellect;
		InventoryIPScript.instance.strength.text = "Str: " + ItemList.instance.onCharacter[index].itemStrenght;
		InventoryIPScript.instance.vitality.text = "Vit: " + ItemList.instance.onCharacter[index].itemVitality;
		InventoryIPScript.instance.itemType.text = ItemList.instance.onCharacter[index].itemType.ToString();
		InventoryIPScript.instance.itemLevel.text = ItemList.instance.onCharacter[index].itemLevel.ToString();
		InventoryIPScript.instance.firstFeature.text = "" + ItemList.instance.onCharacter[index].skill;
		InventoryIPScript.instance.secondFeature.text = "" + ItemList.instance.onCharacter[index].critChance;
	}

	void infoPanel()
	{
		InventoryIPScript.instance.info.PlayForward();
		InventoryIPScript.instance.fade.PlayForward();
	}

	public void HeadEquip()
	{
		if(isHeadEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[headIndex].itemAtlas];
			icon[0].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[headIndex].itemAtlas];
			icon[0].spriteName = ItemList.instance.onCharacter[headIndex].itemIcon;
			frame[0].spriteName = ItemList.instance.onCharacter[headIndex].itemQuality.ToString();
			slot[0].normalSprite = ItemList.instance.onCharacter[headIndex].itemQuality.ToString();
		}
		else
		{
			icon[0].spriteName = "Empty";
			frame[0].spriteName = "Common";
			slot[0].normalSprite = "Common";
		}
	}

	public void ShoulderEquip()
	{
		if(isShoulderEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[shoulderIndex].itemAtlas];
			icon[1].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[shoulderIndex].itemAtlas];
			icon[1].spriteName = ItemList.instance.onCharacter[shoulderIndex].itemIcon;
			frame[1].spriteName = ItemList.instance.onCharacter[shoulderIndex].itemQuality.ToString();
			slot[1].normalSprite = ItemList.instance.onCharacter[shoulderIndex].itemQuality.ToString();
		}
		else
		{
			icon[1].spriteName = "Empty";
			frame[1].spriteName = "Common";
			slot[1].normalSprite = "Common";
		}
	}

	public void GlovesEquip()
	{
		if(isGlovesEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[glovesIndex].itemAtlas];
			icon[2].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[glovesIndex].itemAtlas];
			icon[2].spriteName = ItemList.instance.onCharacter[glovesIndex].itemIcon;
			frame[2].spriteName = ItemList.instance.onCharacter[glovesIndex].itemQuality.ToString();
			slot[2].normalSprite = ItemList.instance.onCharacter[glovesIndex].itemQuality.ToString();
		}
		else
		{
			icon[2].spriteName = "Empty";
			frame[2].spriteName = "Common";
			slot[2].normalSprite = "Common";
		}
	}

	public void BootsEquip()
	{
		if(isBootsEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[bootsIndex].itemAtlas];
			icon[3].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[bootsIndex].itemAtlas];
			icon[3].spriteName = ItemList.instance.onCharacter[bootsIndex].itemIcon;
			frame[3].spriteName = ItemList.instance.onCharacter[bootsIndex].itemQuality.ToString();
			slot[3].normalSprite = ItemList.instance.onCharacter[bootsIndex].itemQuality.ToString();
		}
		else
		{
			icon[3].spriteName = "Empty";
			frame[3].spriteName = "Common";
			slot[3].normalSprite = "Common";
		}
	}

	public void NecklaceEquip()
	{
		if(isNecklaceEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[necklaceIndex].itemAtlas];
			icon[4].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[necklaceIndex].itemAtlas];
			icon[4].spriteName = ItemList.instance.onCharacter[necklaceIndex].itemIcon;
			frame[4].spriteName = ItemList.instance.onCharacter[necklaceIndex].itemQuality.ToString();
			slot[4].normalSprite = ItemList.instance.onCharacter[necklaceIndex].itemQuality.ToString();
		}
		else
		{
			icon[4].spriteName = "Empty";
			frame[4].spriteName = "Common";
			slot[4].normalSprite = "Common";
		}
	}

	public void ArmorEquip()
	{
		if(isArmorEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[armorIndex].itemAtlas];
			icon[5].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[armorIndex].itemAtlas];
			icon[5].spriteName = ItemList.instance.onCharacter[armorIndex].itemIcon;
			frame[5].spriteName = ItemList.instance.onCharacter[armorIndex].itemQuality.ToString();
			slot[5].normalSprite = ItemList.instance.onCharacter[armorIndex].itemQuality.ToString();
		}
		else
		{
			icon[5].spriteName = "Empty";
			frame[5].spriteName = "Common";
			slot[5].normalSprite = "Common";
		}
	}

	public void BeltEquip()
	{
		if(isBeltEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[beltIndex].itemAtlas];
			icon[6].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[beltIndex].itemAtlas];
			icon[6].spriteName = ItemList.instance.onCharacter[beltIndex].itemIcon;
			frame[6].spriteName = ItemList.instance.onCharacter[beltIndex].itemQuality.ToString();
			slot[6].normalSprite = ItemList.instance.onCharacter[beltIndex].itemQuality.ToString();
		}
		else
		{
			icon[6].spriteName = "Empty";
			frame[6].spriteName = "Common";
			slot[6].normalSprite = "Common";
		}
	}

	public void LeggingsEquip()
	{
		if(isLeggingsEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[leggingsIndex].itemAtlas];
			icon[7].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[leggingsIndex].itemAtlas];
			icon[7].spriteName = ItemList.instance.onCharacter[leggingsIndex].itemIcon;
			frame[7].spriteName = ItemList.instance.onCharacter[leggingsIndex].itemQuality.ToString();
			slot[7].normalSprite = ItemList.instance.onCharacter[leggingsIndex].itemQuality.ToString();
		}
		else
		{
			icon[7].spriteName = "Empty";
			frame[7].spriteName = "Common";
			slot[7].normalSprite = "Common";
		}
	}

	public void RelicEquip()
	{
		if(isRelicEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[relicIndex].itemAtlas];
			icon[9].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[relicIndex].itemAtlas];
			icon[9].spriteName = ItemList.instance.onCharacter[relicIndex].itemIcon;
			frame[9].spriteName = ItemList.instance.onCharacter[relicIndex].itemQuality.ToString();
			slot[9].normalSprite = ItemList.instance.onCharacter[relicIndex].itemQuality.ToString();
		}
		else
		{
			icon[9].spriteName = "Empty";
			frame[9].spriteName = "Common";
			slot[9].normalSprite = "Common";
		}
	}
		

	public void MainHandEquip()
	{
		if(isMainHandEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[mainHandIndex].itemAtlas];
			icon[8].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[mainHandIndex].itemAtlas];
			icon[8].spriteName = ItemList.instance.onCharacter[mainHandIndex].itemIcon;
			frame[8].spriteName = ItemList.instance.onCharacter[mainHandIndex].itemQuality.ToString();
			slot[8].normalSprite = ItemList.instance.onCharacter[mainHandIndex].itemQuality.ToString();
		}
		else
		{
			icon[8].spriteName = "Empty";
			frame[8].spriteName = "Common";
			slot[8].normalSprite = "Common";
		}
	}

	public void OffhandEquip()
	{
		if(isOffHandEquipped)
		{
			InventoryIPScript.instance.icon.atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[offHandIndex].itemAtlas];
			icon[10].atlas = InventoryIPScript.instance.atlas[ItemList.instance.onCharacter[offHandIndex].itemAtlas];
			icon[10].spriteName = ItemList.instance.onCharacter[offHandIndex].itemIcon;
			frame[10].spriteName = ItemList.instance.onCharacter[offHandIndex].itemQuality.ToString();
			slot[10].normalSprite = ItemList.instance.onCharacter[offHandIndex].itemQuality.ToString();
		}
		else
		{
			icon[10].spriteName = "Empty";
			frame[10].spriteName = "Common";
			slot[10].normalSprite = "Common";
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.A))
		{
			ItemQuality iQ = (ItemQuality)Random.Range(0,5); // рандом качества вещи
			ItemList.instance.index = Random.Range(0, 11); // выбор из ItemDataBase
			ItemList.instance.AddEquipment(ItemList.instance.index, 60, iQ, 1, false);
			list = ItemList.instance;
			print(ItemList.instance.index);
		}
	}

	public void Plus(int i)
	{
		switch (ItemList.instance.onCharacter[i].itemTitleFeature)
		{
		case "Damage":
			PlayerStats.instance.physicalDamageMax += ItemList.instance.onCharacter[i].itemMainFeature;
			PlayerStats.instance.physicalDamageMin = PlayerStats.instance.physicalDamageMax / 2;
			break;
		case "Armor":
			PlayerStats.instance.armor += ItemList.instance.onCharacter[i].itemMainFeature;
			break;
		}
		PlayerStats.instance.agility += ItemList.instance.onCharacter[i].itemAgility;
		PlayerStats.instance.Intellect += ItemList.instance.onCharacter[i].itemIntellect;
		PlayerStats.instance.strength += ItemList.instance.onCharacter[i].itemStrenght;
		PlayerStats.instance.vitality += ItemList.instance.onCharacter[i].itemVitality;
		Stats();
	}

	public void Minus(int i)
	{
		switch (ItemList.instance.onCharacter[i].itemTitleFeature)
		{
		case "Damage":
			PlayerStats.instance.physicalDamageMax -= ItemList.instance.onCharacter[i].itemMainFeature;
			PlayerStats.instance.physicalDamageMin = PlayerStats.instance.physicalDamageMax / 2;
			break;
		case "Armor":
			PlayerStats.instance.armor -= ItemList.instance.onCharacter[i].itemMainFeature;
			break;
		}
		PlayerStats.instance.agility -= ItemList.instance.onCharacter[i].itemAgility;
		PlayerStats.instance.Intellect -= ItemList.instance.onCharacter[i].itemIntellect;
		PlayerStats.instance.strength -= ItemList.instance.onCharacter[i].itemStrenght;
		PlayerStats.instance.vitality -= ItemList.instance.onCharacter[i].itemVitality;
		Stats();
	}
		
	public void Stats()
	{
		statsLabel.text = string.Format(
			"[u][ff6c6cff]Main[-][/u] \nDamage: {0} - {1} \nMagicDMG: {2} - {3} \nArmor: {4}  \nCriticalStrike: {5} \nDodge - {6}" +
			"[u][ff6c6cff]\nElemental Damage[-][/u] Fire: {7} \nWind: {8} \nEarth: {9} \nWater: {10}" +
			"[u][ff6c6cff]\nElemental Defence[-][/u] \nFire: {11} \nWind: {12} \nEarth: {13} \nWater: {14}" +
			"[u][ff6c6cff]\nAttributes[-][/u] \nAgility: {15} \nIntellect: {16} \nStrength: {17} \nVitality: {18}", 
			PlayerStats.instance.physicalDamageMax, PlayerStats.instance.physicalDamageMin, PlayerStats.instance.magicDamageMax, PlayerStats.instance.magicDamageMin, 
			PlayerStats.instance.armor, PlayerStats.instance.critChance, PlayerStats.instance.dodgeChance, PlayerStats.instance.physicalFireDmg, PlayerStats.instance.physicalWindDmg,
			PlayerStats.instance.physicalEarthDmg, PlayerStats.instance.physicalWaterDmg, PlayerStats.instance.fireDef, PlayerStats.instance.windDef, PlayerStats.instance.earthDef, PlayerStats.instance.waterDef,
			PlayerStats.instance.agility, PlayerStats.instance.Intellect, PlayerStats.instance.strength, PlayerStats.instance.vitality);

		//Надо будет расписать как расшифровывается каждый показатель
	}
}
