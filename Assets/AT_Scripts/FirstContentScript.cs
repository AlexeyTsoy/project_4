﻿using UnityEngine;
using System.Collections;

public class FirstContentScript : MonoBehaviour 
{
	public UIGrid equipmentInventory;

	public static FirstContentScript instance;

	void Awake()
	{
		instance = this;
	}

	void OnEnable()
	{
		if(ItemList.instance.equipmentItems.Count > 0)
		{
			foreach (var item in equipmentInventory.GetChildList()) 
			{
				NGUITools.SetActive(item.gameObject, false);
			}

			for (int i = 0; i < ItemList.instance.equipmentItems.Count; i++)
			{
				GameObject obj = PoolingObjectsScript.instance.GetPooledObject();
				obj.GetComponent<EquipmentScript>().index = i;
				obj.SetActive(true);
			}
			equipmentInventory.repositionNow = true;
		}
	}
}
